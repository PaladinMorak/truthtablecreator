cmake_minimum_required(VERSION 3.10...3.21)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
    cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CXX g++)
set(CMAKE_CXX_STANDARD 20)

project(TruthTableCreator VERSION 1.0
                          DESCRIPTION "A simple CLI truth table creator based on an ENBEF grammar utilizing CXX"
                          LANGUAGES CXX)

add_subdirectory(apps)
add_subdirectory(src)

# Testing only available if this is the main app
if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) AND BUILD_TESTING)
  set(CMAKE_BUILD_TYPE DEBUG)
  set(CMAKE_CXX_FLAGS " ${CMAKE_CXX_FLAGS} -g -O0 -Wall --coverage")
  enable_testing()
  add_subdirectory(tests)
endif()
