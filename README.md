# TTC 
A command line interface Truth Table Creator (TTC) for first order logic formulas written in pure C++ without any 3rd party libraries. 

## Usage
The TTC reads in a normal text file, which has to start with the `<START>` token and end with the `<END>` token. The following operators are allowed to be used
```
0,1         - constants
[a-zA-Z]    - variablse
!           - not
&           - and
|           - or
->          - implication
<->         - biconditional
```
for an example see [example.txt](./example.txt)

## Building
Building can be done using CMake, to enable testing set the `-DBUILD_TESTING=ON` flag.
