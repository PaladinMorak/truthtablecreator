#ifndef __SYNTAX_ERROR_EXCEPTION_HPP__
#define __SYNTAX_ERROR_EXCEPTION_HPP__

#include <stdexcept>
#include <string>

/**
 * @brief A class for denoting that there has been a syntax error.
 *
 * @details This class inherits from the std::runtime_error class.
 */
class SyntaxErrorException : public std::runtime_error
{
  public:
    /**
     * @brief Constructor for the SyntaxErrorException class
     *
     * @details Creates a SyntaxErrorException through calling the super constructor
     *          of the std::runtime_error class.
     *
     * @param message String error message.
     */
    explicit SyntaxErrorException(const std::string& message) : std::runtime_error("Syntax Error: " + message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~SyntaxErrorException() noexcept {};

};

#endif
