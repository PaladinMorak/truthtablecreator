#ifndef __FILE_ERROR_HPP__
#define __FILE_ERROR_HPP__

#include <stdexcept>
#include <string>

/**
 * @brief A class for denoting that there was an error with processing the provided file.
 *
 * @details This class inherits from the std::runtime_error class.
 */
class FileErrorException: public std::runtime_error
{
  public:
    /**
     * @brief Constructor for the FileError
     *
     * @details Creates a FileError exceptionx through calling the super constructor
     *          of the std::runtime_error class.
     *
     * @param message String error message.
     */
    explicit FileErrorException(const std::string& message) : std::runtime_error(message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~FileErrorException() noexcept {};

};

#endif
