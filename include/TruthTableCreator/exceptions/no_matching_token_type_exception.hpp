#ifndef __NO_MATCHING_TOKEN_TYPE_EXCEPTION_HPP__
#define __NO_MATCHING_TOKEN_TYPE_EXCEPTION_HPP__

#include <stdexcept>
#include <string>

#include <TruthTableCreator/exceptions/syntax_error_exception.hpp>

/**
 * @brief A class for denoting that there are no matching Token Types for a given spelling.
 *
 * @details This class inherits from the SyntaxErrorException class.
 */
class NoMatchingTokenTypeException : public SyntaxErrorException
{
  public:
    /**
     * @brief Constructor for the NoMatchingTokenTypeException
     *
     * @details Creates a NoMatchingTokenTypeException exceptions through calling the super constructor
     *          of the SyntaxErrorException class.
     *
     * @param message String error message.
     */
    explicit NoMatchingTokenTypeException(const std::string& message) : SyntaxErrorException(message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~NoMatchingTokenTypeException() noexcept {};

};

#endif
