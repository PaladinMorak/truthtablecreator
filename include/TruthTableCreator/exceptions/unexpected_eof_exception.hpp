#ifndef __UNEXPECTED_EOF_EXCEPTION_HPP__
#define __UNEXPECTED_EOF_EXCEPTION_HPP__

#include <stdexcept>
#include <string>

/**
 * @brief A class for denoting that the end of the file was reached while trying to analyze a token.
 *
 * @details This class inherits from the std::runtime_error class.
 */
class UnexpectedEOFException : public std::runtime_error
{
  public:
    /**
     * @brief Constructor for the UnexpctedEOFException
     *
     * @details Creates a UnexpectedEOFException exceptions through calling the super constructor
     *          of the std::runtime_error class.
     *
     * @param message String error message.
     */
    explicit UnexpectedEOFException(const std::string& message) : std::runtime_error(message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~UnexpectedEOFException() noexcept {};

};

#endif
