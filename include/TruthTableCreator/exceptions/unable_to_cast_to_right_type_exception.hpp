/**
 * @author Georg Bettenhausen
 * @date January 2023
 */
#ifndef __UNABLE_TO_CAST_TO_RIGHT_EXPRESSION_TYPE_EXCEPTION_HPP__
#define __UNABLE_TO_CAST_TO_RIGHT_EXPRESSION_TYPE_EXCEPTION_HPP__

#include <stdexcept>
#include <string>

/**
 * @brief A class for denoting that an Expression AST node could not be cast to the right type as indicated by its
 * type member variable.
 *
 * @details This class inherits from the std::runtime_error class.
 */
class UnableToCastToRightExpressionTypeException : public std::runtime_error
{
  public:
    /**
     * @brief Constructor for the UnableToCastToRightExpressionTypeException
     *
     * @details Creates a UnableToCastToRightExpressionTypeException exceptions through calling the super constructor
     *          of the std::runtime_error class.
     *
     * @param message String error message.
     */
    explicit UnableToCastToRightExpressionTypeException(const std::string& message) : std::runtime_error(message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~UnableToCastToRightExpressionTypeException() noexcept {};

};

#endif
