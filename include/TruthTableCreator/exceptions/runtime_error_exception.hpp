#ifndef __RUNTIME_ERROR_HPP__
#define __RUNTIME_ERROR_HPP__

#include <stdexcept>
#include <string>

/**
 * @brief A class for denoting that there was an error during the runtime of the program
 *
 * @details This class inherits from the std::runtime_error class.
 */
class RuntimeErrorException: public std::runtime_error
{
  public:
    /**
     * @brief Constructor for the RuntimeError
     *
     * @details Creates a RuntimeError exception through calling the super constructor
     *          of the std::runtime_error class.
     *
     * @param message String error message.
     */
    explicit RuntimeErrorException(const std::string& message) : std::runtime_error(message) {};

    /**
     * @brief Deconstructor
     *
     * @details virtual to allow for subclassing
     */
    virtual ~RuntimeErrorException() noexcept {};

};

#endif
