/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __PARSER_HPP__
#define __PARSER_HPP__

#include <deque>
#include <memory>
#include <stack>

#include "token.hpp"
#include "TruthTableCreator/exceptions/syntax_error_exception.hpp"
#include "TruthTableCreator/exceptions/runtime_error_exception.hpp"

#include "TruthTableCreator/syntax/ast_node.hpp"
#include "TruthTableCreator/syntax/expression/expression.hpp"
#include "TruthTableCreator/syntax/expression/logic_expression.hpp"
#include "TruthTableCreator/syntax/expression/and_expression.hpp"
#include "TruthTableCreator/syntax/expression/biconditional_expression.hpp"
#include "TruthTableCreator/syntax/expression/binary_expression.hpp"
#include "TruthTableCreator/syntax/expression/const_expression.hpp"
#include "TruthTableCreator/syntax/expression/implication_expression.hpp"
#include "TruthTableCreator/syntax/expression/not_expression.hpp"
#include "TruthTableCreator/syntax/expression/or_expression.hpp"
#include "TruthTableCreator/syntax/expression/unary_expression.hpp"
#include "TruthTableCreator/syntax/expression/variable_expression.hpp"

class Parser
{
    public:
        /**
         * @brief The constructor for the Parser class.
         *
         * @param tokens A stream of tokens in the form of a double-ended queue.
         */
        Parser(std::deque<Token> tokens);

       /**
        * @brief Parses the token stream that was provided to the constructor and returns the root AST node.
        *
        * @returns std::list<Expression*> A list of pointers to LogicExpressions that rperesent those present
        *     in the document.
        * @throws SyntaxError when an unexpected token was encountered.
        */
       std::list<std::shared_ptr<Expression>> parse();

    private:
        /**
         * @brief The double-ended queue of tokens that should be parsed.
         */
        std::deque<Token> tokens_;

        /**
         * @brief The current token that's being analyzed.
         */
        Token current_token_;

        /**
         * @brief The list of expressions that has been genereated by the parser
         */
        std::list<std::shared_ptr<Expression>> logic_expressions_;

        /**
         * @brief The stack of expressions for the current line.
         * @details This stack is used to identify left hand operands and will be pushed onto the logic_expressions 
         * list once an EOL Token is found, provided that only one expression is on the stack (each line shoulde
         * reduce to a logic expression).
         */
        std::stack<std::shared_ptr<Expression>> current_expression_stack_;

        /** 
         * @brief Checks the curren token's type against the given type throwing a Syntax Error if they don't match
         *
         * @throws SyntaxErrorException if the provided TokenType does not match that of the token currently being
         * looked at.
         * @params expected_type The TokenType that the current token should have
         */
        void checkTokenType(TokenType::type expected_type);

        /**
         * @brief Accepts the current token if it is of the provided type and returns the spelling of it.
         *
         * @throws SyntaxErrorException if the provided TokenType does not match that of the token currently being
         * looked at.
         * @params expected_type The TokenType that should be accepted.
         * @returns std::string The spelling of the token that was accepted.
         */
        std::string acceptTokenType(TokenType::type expected_type);

        /**
         * @brief Accepts the current token no matter its type.
         */
        void acceptToken();

        /**
         * @brief Parses a logical expression or variable beginning with the current token and returns a pointer to the
         * created Expression.
         *
         * @details Can deal with parentheses and switches to more specific expression types such as and expressions wherever possible.
         *
         * @returns Expression* A pointer to the created AST node.
         */
        std::shared_ptr<Expression> parseExpression();

        /**
         * @brief Parses a binary expression beginning with the current token as well as the provided left hand operand
         * of the expression and returns a pointer to the created expression.
         *
         * @details Can deal with parentheses and switches to more specific expression types such as and expressions wherever possible.
         *
         * @param Expression* left_hand_operand The left hand operand of the expression
         * @returns BinaryExpression* A pointer to the created expression.
         */
        std::shared_ptr<BinaryExpression> parseBinaryExpression(std::shared_ptr<Expression> left_hand_operand);

        /**
         * @brief Parses an and expression with the given left-hand operand returning a pointer to an and expression object.
         *
         * @param left_hand_operand A pointer to the left hand operand expression that has already been parsed.
         * @returns AndExpression* A pointer to the created and expression.
         */
        std::shared_ptr<AndExpression> parseAndExpression(std::shared_ptr<Expression> left_hand_operand);

        /**
         * @brief Parses a biconditional expression with the given left-hand operand returning a pointer to a biconditional expression object.
         *
         * @param left_hand_operand A pointer to the left hand operand expression that has already been parsed.
         * @returns BiconditionalExpression* A pointer to the created biconditional expression.
         */
        std::shared_ptr<BiconditionalExpression> parseBiconditionalExpression(std::shared_ptr<Expression> left_hand_operand);

        /**
         * @brief Parses a constant expression returning a pointer to a const expression object.
         *
         * @returns ConstExpression* A pointer to the created const expression.
         */
        std::shared_ptr<ConstExpression> parseConstExpression();

        /**
         * @brief Parses an implication expression with the given left-hand operand returning a pointer to an implication expression object.
         *
         * @param left_hand_operand A pointer to the left hand operand expression that has already been parsed.
         * @returns ImplicationExpression* A pointer to the created implication expression.
         */
        std::shared_ptr<ImplicationExpression> parseImplicationExpression(std::shared_ptr<Expression> left_hand_operand);

        /**
         * @brief Parses a not expression returning a pointer to a not expression object.
         *
         * @returns NotExpression* A pointer to the created not expression.
         */
        std::shared_ptr<NotExpression> parseNotExpression();

        /**
         * @brief Parses an or expression with the given left-hand operand returning a pointer to an or expression object.
         *
         * @param left_hand_operand A pointer to the left hand operand expression that has already been parsed.
         * @returns OrExpression* A pointer to the created or expression.
         */
        std::shared_ptr<OrExpression> parseOrExpression(std::shared_ptr<Expression> left_hand_operand);

        /**
         * @brief Parses a variable returning a pointer to a variable expression object.
         *
         * @returns VariableExpression* A pointer to the created variable.
         */
        std::shared_ptr<VariableExpression> parseVariable();

        /**
         * @brief Checks and returns a boolean whether the provided token is of type END.
         *
         * @param token The token to check.
         * @returns bool whether or not the token is an end token.
         */
         bool isEndToken(Token token) const;

        /**
         * @brief Checks and returns a boolean whether the provided token is of type EOL.
         *
         * @param token The token to check.
         * @returns bool whether or not the token is an EOL token.
         */
         bool isEOLToken(Token token) const;
};
#endif
