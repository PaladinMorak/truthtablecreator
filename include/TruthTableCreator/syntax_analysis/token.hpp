/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __TOKEN_HPP__
#define __TOKEN_HPP__

#include <string>
#include <ostream>

#include "TruthTableCreator/syntax_analysis/token_type.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

class Token
{
    public:
        /**
         * @brief Default constructor for a Token, which creates a Token object with sane defaults
         *
         * @detauls sets the type_ to TokenType::START, spelling_ to an empty string and line_ and column_ to 0
         */
        Token();

        /**
         * @brief Creates a Token object of the given type and spelling.
         *
         * @param type The type of token that should be created.
         * @param spelling The token's spelling.
         * @param line The line that the token occurs on.
         * @param column The column that the token occurs on.
         * @return A Token Object.
         */
        Token(TokenType::type type, std::string spelling, unsigned short line, unsigned short column);

        /**
         * @brief The Deconstructor of a Token
         */
        ~Token(){};

        /**
         * @brief Conversion from Token to string. Returns the toke type followed by the spelling.
         *
         * @details prints the following string "TYPE: SPELLING l: LINE c: COLUMN"
         * @returns std::string the string representation of the token.
         */
        operator std::string() const;

        /**
         * @brief Checks whether or not two tokens are equal returning a bool stating said fact.
         *
         * @param token The token to check equality to.
         * @returns bool A boolean representing whether this token equals the given token.
         */
         bool equals(const Token token) const;

        /**
         * @brief Returns the type of the token
         *
         * @returns TokenType::type the type of the token
         */
         TokenType::type getTokenType() const;

        /**
         * @brief Returns the spelling of the token
         *
         * @returns std::string the spelling of the token
         */
         std::string getTokenSpelling() const;

        /**
         * @brief Returns the line of the token
         *
         * @returns int the line of the token
         */
         int getTokenLine() const;

        /**
         * @brief Returns the column of the token
         *
         * @returns int the column of the token
         */
         int getTokenColumn() const;

        /**
         * @brief Returns the Source Location of the token.
         *
         * @returns SourceLocaiton A source location object detailing where in the code the token is located.
         */
        SourceLocation getSourceLocation() const;

        /**
         * @brief defines the == operator for the token class
         *
         * @param Token& lhs Reference to the lhs token.
         * @param Token& rhs Reference to the rhs token.
         * @returns bool representing whether the left hand tokens equals the right hand side token.
         */
        friend bool operator ==(const Token& lhs, const Token& rhs);

        /**
         * @brief defines the << operator for the token class.
         *
         * @param std::ostream& os Reference to the ostream to place the string into.
         * @param Token const& token Reference to the token to place into the ostream.
         * @returns an ostream with the token placed into it.
         */
        friend std::ostream& operator <<( std::ostream& os, Token const& token );
    private:
        /**
         * @brief The type of the actual token.
         */
        TokenType::type type_;

        /**
         * @brief The spelling of the actual token.
         */
        std::string spelling_;

        /**
         * @brief The line that the token occurs on.
         *
         * Defaults to 0.
         */
        unsigned short line_ = 0;

        /**
         * @brief The column that the token occurs on.
         *
         * Defaults to 0.
         */
        unsigned short column_ = 0;
};

#endif
