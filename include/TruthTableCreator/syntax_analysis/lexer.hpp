/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __LEXER_HPP__
#define __LEXER_HPP__

#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <queue>

#include "token.hpp"
#include "token_type.hpp"
#include "TruthTableCreator/exceptions/no_matching_token_type_exception.hpp"

/**
 * @brief The Lexer class is responsible for generating a Token Stream out of a character input.
 *
 * @details Creates a deque of Tokens that can be parsed. This is a static class and does not allow for utilizing a constructor.
 */
class Lexer final
{
    public:
        /**
         * @brief Creates a Lexer object based on the given ifstream representing a file
         *
         * @throws FileErrorException when the input stream is not "good"
         * @param std::ifstream The pointer of the file to be read and lexed
         * @returns Lexer The lexer object based istream representing a file
         */
        Lexer(std::ifstream *input_file);

        /**
         * @brief Lexes the given input file and returns a queue of deque of tokens representing said file.
         *
         * @returns std::deque<Token> A deque of tokens representing the input file.
         */
         std::deque<Token> scan();

    private:
        /**
         * @brief The current Line scanned, defaults to 1.
         */
        int current_line_ = 1;

        /**
         * @brief The current Column scanned, defaults to 0.
         */
        int current_column_ = 0;

        /**
         * @brief The last line scanned, defaults to 1
         */
        int last_line_ = 1;

         /**
          * @brief The last column scanned, defaults to 0.
          */
        int last_column_ = 0;

        /**
         * @brief the current char that is being lexed.
         */
         char current_char_;

        /**
         * @brief the current spelling being lexed.
         */
         std::string current_spelling_;

        /**
         * @brief the input stream used internally to lex the file
         */
         mutable std::ifstream *input_file_;

        /**
         * @brief Skips the current character without analyzing, gets the next character in the file and advances the
         *     necessary line/column counters.
         * @throws UnexpectedEOFError when asked to skip a character and getting the next character is not possible
         *     due to it being the EOF.
         */
        void skipChar();

        /**
         * @brief Adds the current character to the current spelling.
         * @throws UnexepctedEOFError when reaching the end of the file unexpectedly
         */
         void takeChar();

        /**
         * @brief scans the current Token and tries to find a token type that matches the current spelling and
         *     returns it.
         *
         * @returns TokenType::type the type of the scanned token
         */
         TokenType::type scanToken();

        /**
         * @brief Checks if the given char is a letter [a-zA-Z] and returns a boolean representing said property.
         *
         * @params char The character to be checked.
         * @returns bool Representing whether the given char is a letter.
         */
         bool isLetter(char character);

        /**
         * @brief Checks if the given char is a digit and returns a boolean representing said property.
         *
         * @params char The character to be checked.
         * @returns bool Representing whether the given char is a digit.
         */
         bool isDigit(char character);

        /**
         * @brief Sub function of scanToken specifically for scanning variables.
         *
         * @returns TokenType::type The token type represented, should always be TokenType::VAR.
         */
         TokenType::type scanVariable();

        /**
         * @brief Sub function of scanToken specifically for scanning biconditionals.
         *
         * @returns TokenType::type The token type represented, should always be TokenType::BICOND.
         */
         TokenType::type scanBiConditional();

        /**
         * @brief Sub function of scanToken specifically for scanning the start token.
         *
         * @returns TokenType::type The token type represented, should always be TokenType::START.
         */
         TokenType::type scanStart();

        /**
         * @brief Sub function of scanToken specifically for scanning the end token.
         *
         * @returns TokenType::type The token type represented, should always be TokenType::END.
         */
         TokenType::type scanEnd();
};

#endif
