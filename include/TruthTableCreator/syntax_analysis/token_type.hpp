/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __TOKEN_TYPE_HPP__
#define __TOKEN_TYPE_HPP__

#include <string>

/**
* @brief An enum item that contains the different available Token Types
*/
class TokenType
{
    public:
        enum type{
            VAR, // can be any string starting with an upper or lower case letter of the english alphabet and containing
                 // upper or lower case ltters as well as numbers
            TRUE, // 1
            FALSE, //0
            NOT, // !
            AND, // &
            OR, // |
            IMPL, // ->
            BICOND, // <->
            LPAREN, // (
            RPAREN, // )
            START, // Start of file
            END, // End of file
            EOL, // End of line
        };

        /**
        * @brief Finds the correct TokenType for the given spelling.
        *
        * @details If no TokenType can be found matching the spelling this methods throws an NoMatchingTokenTypeException exception.
        *
        * @param spelling The spelling of the Token for which the type is to be found.
        * @return The matching TokenType to the spelling if one can be found.
        * @throws NoMatchingTokenTypeException If no matching Token can be found to the given spelling.
        */
        static TokenType::type spellingToTokenType(std::string spelling);

        /**
         * @brief Converts the given type to a string that can be used for debugging purposes
         *
         * @param TokenType::type  typeToConvert The type that should be converted
         */
         static std::string typeToString(TokenType::type typeToConvert);
};

#endif
