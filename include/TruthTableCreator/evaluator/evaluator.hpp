/**
 * @author Georg Bettenhausen
 * @date April 2024
 */

#include <vector>
#include <map>
#include <memory>

#include <TruthTableCreator/syntax/expression/expression.hpp>
#include <TruthTableCreator/exceptions/runtime_error_exception.hpp>

/**
 * @brief The evaluator class is used to evaluate the expression node into real 
 * world logic that follows the logic described by the node.
 */
class Evaluator
{
    public:
        /**
         * @brief evaluates the provided expression returning a map object that 
         * maps the variable inputs to the output.
         *
         * @param expression The expression to evaluate
         * @returns std::map a map linking the variable mappings to the boolean 
         * output.
         */
        static std::map<std::map<std::string, bool>, bool> evaluateExpression(std::shared_ptr<Expression> expression);

        /**
         * @brief Evaluates the given expression using the given input.
         *
         * @details Throws a MissingValueError if not all variables present in
         * the expression were provided with a boolean value.
         *
         * @throws RuntimeErrorException if it encounters a variable for which
         * no mapping exists.
         *
         * @param expression The expression to evaluate
         * @param variable_mappings The mappings from variable names to boolean values
         *
         *
         * @returns bool The boolean result of evaluating the expression
         */
        static bool evaluateExpressionFor(std::shared_ptr<Expression> expression, 
                std::map<std::string, bool> variable_mappings);
        /**
         * @brief Finds all of the variables in the given expression and appends
         * them to the provided vector.
         *
         * @param expression The exrpession to check variables in.
         * @param variable_names The vector to add the variable name to
         * @returns a vector of variable names.
         */
        static void findVariables(std::shared_ptr<Expression> expression,
                std::vector<std::string> &variable_names);

        /**
         * @brief Generates cross product of possible variable mappings for the
         * given vector of variable names.
         *
         * @param variable_names The vector of variable names.
         * @returns std::map<std::map<std::string, bool>, bool> possible mappings 
         */
        static std::map<std::map<std::string, bool>, bool> generateVariableMappings(std::vector<std::string> variables);

};
