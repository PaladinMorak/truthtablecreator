/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __NODE_TYPE_HPP__
#define __NODE_TYPE_HPP__

#include <string>

class NodeType
{
    public:
        enum type{
            Node, // default type for an AstNode
            Expression,
            UnaryExpression,
            BinaryExpression,
            ConstExpression,
        };

        /**
         * @brief Converts the given type to a string that can be used for debugging purposes
         *
         * @param NodeType::type  typeToConvert The type that should be converted
         */
         static std::string typeToString(NodeType::type typeToConvert);
};
#endif
