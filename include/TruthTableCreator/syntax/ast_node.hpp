/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __AST_NODE_HPP__
#define __AST_NODE_HPP__

#include <memory>

#include "source_location.hpp"
#include "node_type.hpp"

/**
 * @brief The AstNode class is used to represent an Abstract Syntax Tree (AST) node.
 */

class AstNode
{
    public:
        /**
         * @brief The default constructor for an AstNode which defaults on the SourceLocation constructor.
         */
        AstNode();

        /**
         * @brief The constructor for an AstNode, which takes the source code location in line, column format.
         *
         * @param location The source location of the AST node.
         */
        AstNode(SourceLocation location);

        /**
         * @brief Returns the Source Location of the ASTNode as a tuple of (line, column).
         *
         * @returns SourceLocation The source location of the AstNode as a SourceLocation object.
         */
        SourceLocation getSourceLocation() const;

        /**
         * @brief Checks and Returns a bool representing whether or not this AST node is equal to the given one.
         *
         * @param node Pointer to the AstNode to check equality against.
         * @returns bool Representing whether or not the two objects are equal.
         */
         virtual bool equals(std::shared_ptr<AstNode> node) const;

        /**
         * @brief Conversion from AstNode to string. Returns "line: X, column: Y"
         * @returns std::string the string representation of the AstNode.
         */
        virtual operator std::string() const;

        /**
         * @brief Conversion from this AstNode to string.
         * @details Returns "line: X, column: Y".
         *
         * @returns std::string the string representation of the Expression.
         */
        virtual std::string toString() const;

        /**
         * @brief Defines the << operator for the AstNode class.
         * @details Utilizes the std::string function defined earlier in the class.
         *
         * @param os The output stream to append the location to.
         * @param location The AstNode to append.
         * @returns std::ostream& A reference to the combined output stream.
         */
        friend std::ostream& operator <<( std::ostream& os, AstNode const& node );

        /**
         * @brief Returns the type of the node.
         *
         * @returns NodeType::type the type of the node.
         */
        NodeType::type getType() const;

    private:
        /**
         * @brief The source location of the AstNode.
         */
        SourceLocation source_location_;

    protected:
        /**
         * @brief The type of the node
         */
        NodeType::type type_;
};

#endif
