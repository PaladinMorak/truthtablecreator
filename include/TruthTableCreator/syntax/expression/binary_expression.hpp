/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __BINARY_EXPRESSION_HPP__
#define __BINARY_EXPRESSION_HPP__

#include "expression.hpp"

/**
 * @brief An AST node representing a binary expression in the source code.
 */
class BinaryExpression : public Expression
{
  public:
    /**
     * @brief Default Constructor for the Binary Expression AST node, denoting an expression with two operands.
     * @details Simply calls the super constructor for the member variables and sets the correct type.
     */
     BinaryExpression();

    /**
     * @brief Constructor for the Binary Expression AST node, denoting an expression with two operands.
     *
     * @param source_location The source location of the binary expression.
     * @param left_hand_operand The left hand operand of the binary expression.
     * @param right_hand_expression The right hand operand of the binary expression.
     */
     BinaryExpression(SourceLocation source_location, std::shared_ptr<Expression> left_hand_operand, std::shared_ptr<Expression> right_hand_operand);

    /**
     * @brief virtual Deconstructor
     */
    virtual ~BinaryExpression(){};

    /**
     * @brief Returns the left hand operand as a pointer
     * @returns Expression representing the left hand operand.
     */
    std::shared_ptr<Expression> getLeftHandOperand() const;

    /**
     * @brief Returns the right hand operand
     * @returns Expression representing the right hand operand.
     */
     std::shared_ptr<Expression> getRightHandOperand() const;

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     * @details Overrides the equals function defined in the Expression class.
     *
     * @param expr Pointer to the expression object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     */
    bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from BinaryExpression to string.
     * @details Returns "BinaryExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the BinaryExpression.
     */
    operator std::string() const override;

    /**
     * @brief Conversion from BinaryExpression to string.
     * @details Returns "BinaryExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the BinaryExpression.
     */
    std::string toString() const override;

  protected:
    /**
     * @brief The left hand operand of the expression.
     */
    std::shared_ptr<Expression> left_hand_operand_;

    /**
     * @brief The right hand operand of the expression.
     */
    std::shared_ptr<Expression> right_hand_operand_;

};
#endif
