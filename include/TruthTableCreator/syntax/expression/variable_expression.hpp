/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#ifndef __VARIABLE_HPP__
#define __VARIABLE_HPP__

#include "TruthTableCreator/syntax/expression/expression.hpp"
#include "string.h"

/**
 * @brief An AST node representing a variable in the source code.
 */
 class VariableExpression: public Expression
 {
     public:
        /**
         * @brief Default Constructor for the Variable AST node, denoting a variable.
         * @details Simply calls the super constructor for the member variables, sets the correct type, and sets the
         * variable spelling to an empty string.
         */
        VariableExpression();

        /**
         * @brief Main Constructor for the Variable AST node, denoting a variable.
         *
         * @param source_location The source location of the variable.
         * @param spelling The spelling of the variable.
         */
        VariableExpression(SourceLocation source_location, std::string spelling);

        /**
         * virtual deconstructor
         */
        virtual ~VariableExpression(){};

        /**
         * @brief Returns the spelling of the variable as a string.
         *
         * @returns std::string The spelling of the variable.
         */
        std::string getSpelling() const;

        /**
         * @brief Returns whether or not this variable is equal to the given one.
         * @details Compares the Spellings to determine, whether or not the variables are equal.
         *
         * @param variable The variable to compare to.
         * @returns bool Representing whether or not the given variable is equal to this object.
         */
        bool equals(std::shared_ptr<AstNode> variable) const override;

        /**
         * @brief Returns a string describing the Variable object.
         * @details The string has the following form "Variable, Source Location: X; Spelling: Y".
         *
         * @returns std::string Representing the variable object.
         */
        operator std::string() const override;

        /**
         * @brief Returns a string describing the Variable object.
         * @details The string has the following form "Variable, Source Location: X; Spelling: Y".
         *
         * @returns std::string Representing the variable object.
         */
        std::string toString() const override;

        /**
         * @brief returns a "prettified" string of the expression that uses the 
         * short hand notation, such as a for a variable
         *
         * @returns std::string the prettified string representation of the expression.
         */
        virtual std::string toPrettyString() const override;

    private:
        /**
         * @brief A string representing the spelling of the variable.
         */
        std::string spelling_;
 };

 #endif
