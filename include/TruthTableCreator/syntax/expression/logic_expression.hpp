/**
 * @author Georg Bettenhausen
 * @date July 2023
 */
#ifndef __LOGIC_EXPRESSION_HPP__
#define __LOGIC_EXPRESSION_HPP__

#include "TruthTableCreator/syntax/ast_node.hpp"
#include <list>
#include <string>

/**
 * @brief An AstNode that represents a first order logic expression.
 *
 * @details Will always be used as the root node of the AST, and holds the variables used in it.
 */

class LogicExpression : public AstNode
{
    public:
        /**
         * @brief The Constructor for a Logic Expression class object.
         *
         * @details Takes a source location, which will always be a 0 column and an X line, and a list of strings
         * representing the variables in order to make the truth table generation easier, by quickly having access
         * to all variables used in the logic expression.
         *
         * @param source_location The source location of the logical expression
         * @param variables A list of strings representing the variables used in the logic expression.
         */
        LogicExpression(SourceLocation source_location, std::list<std::string> variables);

        /**
         * virtual deconstructor
         */
        virtual ~LogicExpression(){};

        /**
         * @brief Returns a list of strings representing the variables used throughout the logic expression.
         *
         * @returns std::list<std::string> List of variables used throughout the logic expression.
         */
        std::list<std::string> getVariables() const;

    private:
        /**
         * @brief The instance field of the logic expression holding a list of variables used throughout it.
         */
        std::list<std::string> variables_;
};

#endif
