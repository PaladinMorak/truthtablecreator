/**
 * @author Georg Bettenhausen
 * @date June 2022
 */

#ifndef __IMPLICATION_EXPRESSION_HPP__
#define __IMPLICATION_EXPRESSION_HPP__

#include "binary_expression.hpp"
#include "expression.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

/**
 * @brief The ImplicationExpression class represents a binary IMPLICATION expression in the AST.
 */
class ImplicationExpression : public BinaryExpression
{
  public:
    /**
     * @brief Default Constructor for an and expression AST node.
     */
    ImplicationExpression();

    /**
     * @brief Constructor for an and expression AST node.
     *
     * @param source_location The source location of the and expression.
     * @param leftHandOperand The left hand operand of the logical IMPLICATION expression.
     * @param rightHandOperand The right hand operand of the locgical IMPLICATION expression.
     */
    ImplicationExpression(SourceLocation source_location, std::shared_ptr<Expression> left_hand_operand, std::shared_ptr<Expression> right_hand_operand);

    /**
     * virtual deconstructor
     */
    virtual ~ImplicationExpression(){};

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     * @details Overrides the equals function defined in the Expression class.
     *
     * @param expr Pointer to the expression object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     */
    bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from ImplicationExpression to string.
     * @details Returns "ImplicationExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the ImplicationExpression.
     */
    operator std::string() const override;

    /**
     * @brief Conversion from ImplicationExpression to string.
     * @details Returns "ImplicationExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the ImplicationExpression.
     */
    std::string toString() const override;

    /**
     * @brief returns a "prettified" string of the expression that uses the 
     * short hand notation, such as -> for the implication operator
     *
     * @returns std::string the prettified string representation of the expression.
     */
    virtual std::string toPrettyString() const override;
};

#endif
