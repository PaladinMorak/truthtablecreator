/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __EXPRESSION_HPP__
#define __EXPRESSION_HPP__

#include "TruthTableCreator/exceptions/unable_to_cast_to_right_type_exception.hpp"
#include "TruthTableCreator/syntax/ast_node.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

/**
 * @brief An AST node representing an expression in the source code.
 */
class Expression : public AstNode
{
  public:
    /**
     * @brief The default constructor for an Expression AST node, simply calls the super constructor of AstNode.
     */
    Expression();

    /**
     * @brief The Constructor for an Expression AST node, simply calls the super constructor of AstNode.
     *
     * @param source_location The source location of the expression in the source code
     */
    Expression(SourceLocation source_location);

    /**
     * virtual deconstructor
     */
    virtual ~Expression(){};

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     *
     * @param expr Pointer to the Expression object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     * @throws UnableToCastToRightTypeException Representing that errors occurred while trying to cast objects
     *                                          to the type indicated by their member variable.
     */
    virtual bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from Expression to string.
     * @details Returns "Expression, Source Location: X".
     *
     * @returns std::string the string representation of the Expression.
     */
    operator std::string() const override;

    /**
     * @brief Conversion from Expression to string.
     * @details Returns "Expression, Source Location: X".
     *
     * @returns std::string the string representation of the Expression.
     */
    virtual std::string toString() const override;

    /**
     * @brief returns a "prettified" string of the expression that uses the 
     * short hand notation, such as & for the and operator
     *
     * @returns std::string the prettified string representation of the expression.
     */
    virtual std::string toPrettyString() const;
    
    /**
     * @brief Defines the << operator for the Expression class.
     * @details Utilizes the toString function defined earlier in the class.
     *
     * @param os The output stream to append the location to.
     * @param location The Expression to append.
     * @returns std::ostream& A reference to the combined output stream.
     */
    friend std::ostream& operator <<( std::ostream& os, Expression const& node );
};

#endif
