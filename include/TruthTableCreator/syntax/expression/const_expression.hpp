/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __CONST_EXPRESSION_HPP__
#define __CONST_EXPRESSION_HPP__

#include "expression.hpp"

/**
 * @brief The ConstExpression class denotes a constant expression, which can either be 0 (false) or 1 (true).
 */

class ConstExpression : public Expression
{
  public:
    /**
     * @brief The default constructor for a Constant Expression, defaults the value to false.
     * @details Calls the default Expression constructor.
     */
    ConstExpression();

    /**
     * @brief The main constructor for the Constant Expression, which sets the value to the provided one.
     * @details Calls the Expression constructor.
     *
     * @param source_location The source location of the expression.
     * @param value The boolean value of the expression.
     */
    ConstExpression(SourceLocation source_location, bool value);

    /**
     * virtual deconstructor
     */
    virtual ~ConstExpression(){};

    /**
     * @brief Returns the value of the constant expression.
     *
     * @returns bool Representing the value of the constant expression.
     */
     bool getValue() const;

    /**
     * @brief Checks whether this constant expression is equal to the given one and returns a bool representing this.
     *
     * @param expression The expression to check equality against.
     * @returns bool Representing whether or not the two expressions are equal.
     */
    bool equals(std::shared_ptr<AstNode> expression) const override;

    /**
     * @brief Conversion from ConstExpression to string.
     * @details Returns "ConstExpression, Source Location: X; Value: Y".
     *
     * @returns std::string the string representation of the ConstExpression.
     */
    operator std::string() const override;

    /**
     * @brief Conversion from ConstExpression to string.
     * @details Returns "ConstExpression, Source Location: X; Value: Y".
     *
     * @returns std::string the string representation of the ConstExpression.
     */
    std::string toString() const override;

    /**
     * @brief returns a "prettified" string of the expression that uses the 
     * short hand notation, such as 1 or 0
     *
     * @returns std::string the prettified string representation of the expression.
     */
    virtual std::string toPrettyString() const override;

  private:
    bool value_;
};

#endif
