/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#ifndef __UNARY_EXPRESSION_HPP__
#define __UNARY_EXPRESSION_HPP__

#include <memory>

#include "expression.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

/**
 * @brief The UnaryExpression class is a child class of the Expression class and represents an Expression with only one
 * operand in the AST.
 */
class UnaryExpression : public Expression
{
  public:
    /**
     * @brief The default constructor for a unary expression, which requires no arguments.
     */
    UnaryExpression();

    /**
     * @brief The main constructor for the UnaryExpression class, which takes the source location and operand as inputs.
     *
     * @param source_location the location in the source code, where the expression occurs.
     * @param operand the operand of the expression.
     */
     UnaryExpression(SourceLocation source_location, std::shared_ptr<Expression> operand);

    /**
     * @brief virtual Deconstructor
     */
    virtual ~UnaryExpression(){};

    /**
     * @brief Returns the operand of the unary expression
     * @returns Expression the operand of the unary expression
     */
    std::shared_ptr<Expression> getOperand() const;

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     * @details Overrides the equals function defined in the Expression class.
     *
     * @param expr Pointer to the AstNode object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     */
    bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from UnaryExpression to string.
     * @details Returns "UnaryExpression, Source Location: X; Operand: Y"
     *
     * @returns std::string the string representation of the UnaryExpression.
     */
    virtual operator std::string() const override;

    /**
     * @brief Conversion from UnaryExpression to string.
     * @details Returns "UnaryExpression, Source Location: X; Operand: Y"
     *
     * @returns std::string the string representation of the UnaryExpression.
     */
    virtual std::string toString() const override;

  protected:
    /**
     * @brief the operand of the unary expression.
     */
    std::shared_ptr<Expression> operand_;
};

#endif
