/**
 * @author Georg Bettenhausen
 * @date June 2022
 */

#ifndef __BICONDITIONAL_EXPRESSION_HPP__
#define __BICONDITIONAL_EXPRESSION_HPP__

#include "binary_expression.hpp"
#include "expression.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

/**
 * @brief The BiconditionalExpression class represents a binary BICONDITIONAL expression in the AST.
 */
class BiconditionalExpression : public BinaryExpression
{
  public:
    /**
     * @brief Default Constructor for an and expression AST node.
     */
    BiconditionalExpression();

    /**
     * @brief Constructor for an and expression AST node.
     *
     * @param source_location The source location of the and expression.
     * @param leftHandOperand The left hand operand of the logical BICONDITIONAL expression.
     * @param rightHandOperand The right hand operand of the locgical BICONDITIONAL expression.
     */
    BiconditionalExpression(SourceLocation source_location, std::shared_ptr<Expression> leftHandOperand, std::shared_ptr<Expression> rightHandOperand);

    /**
     * virtual deconstructor
     */
    virtual ~BiconditionalExpression(){};

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     * @details Overrides the equals function defined in the Expression class.
     *
     * @param expr Pointer to the expression object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     */
    bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from BiconditionalExpression to string.
     * @details Returns "BiconditionalExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the BiconditionalExpression.
     */
    operator std::string() const override;

    /**
     * @brief Conversion from BiconditionalExpression to string.
     * @details Returns "BiconditionalExpression, Source Location: X; Left-Hand Operand: Y; Right-Hand Operand: Z".
     *
     * @returns std::string the string representation of the BiconditionalExpression.
     */
    std::string toString() const override;

    /**
     * @brief returns a "prettified" string of the expression that uses the 
     * short hand notation, such as <-> for the biconditional operator
     *
     * @returns std::string the prettified string representation of the expression.
     */
    virtual std::string toPrettyString() const override;
};

#endif
