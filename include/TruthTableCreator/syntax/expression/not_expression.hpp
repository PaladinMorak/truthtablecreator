/**
 * @author Georg Bettenhausen
 * @date November 2022
 */
#ifndef __NOT_EXPRESSION_HPP__
#define __NOT_EXPRESSION_HPP__

#include "expression.hpp"
#include "unary_expression.hpp"
#include "TruthTableCreator/syntax/source_location.hpp"

/**
 * @brief The NotExpression class is a UnaryExpression that represents the logical not operator/expression.
 */
class NotExpression : public UnaryExpression
{
  public:
    /**
     * @brief The default constructor of the NotExpression class with no inputs.
     * @details Simply calls the default constructor of the UnaryExpression class.
     */
    NotExpression();

    /**
     * @brief The main constructor of the NotExpression that should be used, with two inputs the location and operand.
     *
     * @param source_location the source location of the expression.
     * @param operand the operand of the expression.
     */
    NotExpression(SourceLocation source_location, std::shared_ptr<Expression> operand);

    /**
     * virtual deconstructor
     */
    virtual ~NotExpression(){};

    /**
     * @brief Checks and returns, a bool, representing whether this object and the given one are equal.
     * @details Overrides the equals function defined in the Expression class.
     *
     * @param expr Pointer to the AstNode object to check equality to.
     * @returns bool Represents whether or not this object is equal to the given one.
     */
    bool equals(std::shared_ptr<AstNode> expr) const override;

    /**
     * @brief Conversion from NotExpression to string.
     * @details Returns "NotExpression, Source Location: X; Operand: Y"
     *
     * @returns std::string the string representation of the NotExpression.
     */
    virtual operator std::string() const override;

    /**
     * @brief Conversion from NotExpression to string.
     * @details Returns "NotExpression, Source Location: X; Operand: Y"
     *
     * @returns std::string the string representation of the NotExpression.
     */
    std::string toString() const override;

    /**
     * @brief returns a "prettified" string of the expression that uses the 
     * short hand notation, such as ! for the not operator
     *
     * @returns std::string the prettified string representation of the expression.
     */
    virtual std::string toPrettyString() const override;
};

#endif
