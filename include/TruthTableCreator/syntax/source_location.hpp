#ifndef __SOURCE_LOCATION_HPP__
#define __SOURCE_LOCATION_HPP__

/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <string>
#include <ostream>

/**
 * @brief The SourceLocation class is used to denote a location in the source code.
 */

class SourceLocation
{
    public:
        /**
         * @brief Default SourceLocation constructor, which doesn't require any input.
         *
         * @details Simply sets the line and column to 0.
         */
        SourceLocation();

        /**
         * @brief The SourceLocation constructor which takes one unsigned short for both the line and column index.
         *
         * @param line The line of the source location.
         * @param column The column of the source location.
         */
        SourceLocation(unsigned short line, unsigned short column);

        /**
         * @brief Returns the line of the source location.
         *
         * @return unsigned short representing the line of the source location.
         */
        unsigned short getLine() const;

        /**
         * @brief Returns the column of the source location.
         *
         * @return unsigned short representing the column of the source location.
         */
        unsigned short getColumn() const;

        /**
         * @brief  Checks whether this SourceLocation object is equal to the given one.
         * @details Two SourceLocation objects are equal when both line and column number are equal.
         *
         * @param location the SourceLocation to check equality against.
         * @returns bool representing whether or not the two objects are equal.
         */
        bool equals(SourceLocation location) const;

        /**
         * @brief Defines the == operator for the SourceLocation class.
         * @details Utilizes the equals method of the SourceLocation class.
         *
         * @param lhs The left hand side SourceLocation.
         * @param rhs The right hand side SourceLocation.
         */
        friend bool operator ==(SourceLocation const& lhs, SourceLocation const& rhs);

        /**
         * @brief Conversion from Source Location to string. Returns "line: X, column: Y"
         * @returns std::string the string representation of the source location.
         */
        operator std::string() const;

        /**
         * @brief Defines the << operator for the SourceLocation class.
         * @details Utilizes the std::string function defined earlier in the class.
         *
         * @param os The output stream to append the location to.
         * @param location The source locationt o append.
         * @returns std::ostream& A reference to the combined output stream.
         */
        friend std::ostream& operator <<( std::ostream& os, SourceLocation const& location );

    private:
        /**
         * @brief The line index of the source location
         */
        unsigned short line_;

        /**
         * @brief The column index of the source location
         */
        unsigned short column_;
};

#endif
