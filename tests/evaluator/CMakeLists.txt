add_executable(test_evaluator test_evaluator.cpp)

target_compile_features(test_evaluator PRIVATE cxx_std_20)

target_link_libraries(test_evaluator PRIVATE libttc_evaluator Catch2::Catch2WithMain)

add_test(NAME evaluator_tests COMMAND test_evaluator)
