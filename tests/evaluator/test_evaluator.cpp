/**
 * @author Georg Bettenhausen
 * @date April 2024
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <map>

#include <TruthTableCreator/syntax/expression/expression.hpp>
#include <TruthTableCreator/syntax/expression/binary_expression.hpp>
#include <TruthTableCreator/syntax/expression/unary_expression.hpp>
#include <TruthTableCreator/syntax/expression/variable_expression.hpp>
#include <TruthTableCreator/syntax/expression/not_expression.hpp>
#include <TruthTableCreator/syntax/expression/and_expression.hpp>
#include <TruthTableCreator/syntax/expression/or_expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>
#include <TruthTableCreator/syntax/expression/implication_expression.hpp>
#include <TruthTableCreator/syntax/expression/biconditional_expression.hpp>
#include <TruthTableCreator/evaluator/evaluator.hpp>

TEST_CASE("Test find variables for variable expression", "[evaluator]")
{
    std::shared_ptr<VariableExpression> test_expression = std::make_shared<VariableExpression>(VariableExpression(SourceLocation(1,0), "test"));

    std::vector<std::string> result = std::vector<std::string>();
    Evaluator::findVariables(test_expression, result);
    std::vector<std::string> expected_result = {"test"};
    REQUIRE(result == expected_result);
}

TEST_CASE("Test find variables for unary expression", "[evaluator]")
{
    std::shared_ptr<NotExpression> test_expression = std::make_shared<NotExpression>(SourceLocation(1,0), std::make_shared<VariableExpression>(VariableExpression(SourceLocation(1,1), "test")));

    std::vector<std::string> result = std::vector<std::string>();
    Evaluator::findVariables(test_expression, result);
    std::vector<std::string> expected_result = {"test"};
    REQUIRE(result == expected_result);
}

TEST_CASE("Test find variables for binary expression", "[evaluator]")
{
    std::shared_ptr<VariableExpression> left_operand = std::make_shared<VariableExpression>(VariableExpression(SourceLocation(1,0), "test"));
    std::shared_ptr<VariableExpression> right_operand = std::make_shared<VariableExpression>(VariableExpression(SourceLocation(1,2), "abcd"));
    std::shared_ptr<AndExpression> test_expression = std::make_shared<AndExpression>(AndExpression(SourceLocation(1,1), left_operand, right_operand));

    std::vector<std::string> result = std::vector<std::string>();
    Evaluator::findVariables(test_expression, result);
    std::vector<std::string> expected_result = {"test", "abcd"};
    REQUIRE(result == expected_result);
}

TEST_CASE("Test find variables for expression without variables", "[evaluator]")
{
    std::shared_ptr<ConstExpression> test_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));

    std::vector<std::string> result = std::vector<std::string>();
    Evaluator::findVariables(test_expression, result);
    std::vector<std::string> expected_result = {};
    REQUIRE(result == expected_result);
}

TEST_CASE("Test const expression evaluation", "[evaluator]")
{
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), false));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(true_expression);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(false_expression);

    REQUIRE((result_1[std::map<std::string, bool>()]) == true);
    REQUIRE((result_2[std::map<std::string, bool>()]) == false);
}

TEST_CASE("Test not expression evaluation", "[evaluator]")
{
    std::shared_ptr<NotExpression> true_expression = std::make_shared<NotExpression>(NotExpression(SourceLocation(1,0), std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,1), false))));
    std::shared_ptr<NotExpression> false_expression = std::make_shared<NotExpression>(NotExpression(SourceLocation(1,0), std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,1), true))));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(true_expression);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(false_expression);

    REQUIRE((result_1[std::map<std::string, bool>()]) == true);
    REQUIRE((result_2[std::map<std::string, bool>()]) == false);
}

TEST_CASE("Test and expression evaluation", "[evaluator]")
{
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), false));

    std::shared_ptr<AndExpression> and_expression_1 = std::make_shared<AndExpression>(AndExpression(SourceLocation(1,0), false_expression, false_expression));
    std::shared_ptr<AndExpression> and_expression_2 = std::make_shared<AndExpression>(AndExpression(SourceLocation(1,0), false_expression, true_expression));
    std::shared_ptr<AndExpression> and_expression_3 = std::make_shared<AndExpression>(AndExpression(SourceLocation(1,0), true_expression, false_expression));
    std::shared_ptr<AndExpression> and_expression_4 = std::make_shared<AndExpression>(AndExpression(SourceLocation(1,0), true_expression, true_expression));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(and_expression_1);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(and_expression_2);
    std::map<std::map<std::string, bool>, bool> result_3 = Evaluator::evaluateExpression(and_expression_3);
    std::map<std::map<std::string, bool>, bool> result_4 = Evaluator::evaluateExpression(and_expression_4);

    REQUIRE((result_1[std::map<std::string, bool>()]) == false);
    REQUIRE((result_2[std::map<std::string, bool>()]) == false);
    REQUIRE((result_3[std::map<std::string, bool>()]) == false);
    REQUIRE((result_4[std::map<std::string, bool>()]) == true);
}

TEST_CASE("Test or expression evaluation", "[evaluator]")
{
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), false));

    std::shared_ptr<OrExpression> or_expression_1 = std::make_shared<OrExpression>(OrExpression(SourceLocation(1,0), false_expression, false_expression));
    std::shared_ptr<OrExpression> or_expression_2 = std::make_shared<OrExpression>(OrExpression(SourceLocation(1,0), false_expression, true_expression));
    std::shared_ptr<OrExpression> or_expression_3 = std::make_shared<OrExpression>(OrExpression(SourceLocation(1,0), true_expression, false_expression));
    std::shared_ptr<OrExpression> or_expression_4 = std::make_shared<OrExpression>(OrExpression(SourceLocation(1,0), true_expression, true_expression));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(or_expression_1);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(or_expression_2);
    std::map<std::map<std::string, bool>, bool> result_3 = Evaluator::evaluateExpression(or_expression_3);
    std::map<std::map<std::string, bool>, bool> result_4 = Evaluator::evaluateExpression(or_expression_4);

    REQUIRE((result_1[std::map<std::string, bool>()]) == false);
    REQUIRE((result_2[std::map<std::string, bool>()]) == true);
    REQUIRE((result_3[std::map<std::string, bool>()]) == true);
    REQUIRE((result_4[std::map<std::string, bool>()]) == true);
}

TEST_CASE("Test implication expression evaluation", "[evaluator]")
{
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), false));

    std::shared_ptr<ImplicationExpression> impl_expression_1 = std::make_shared<ImplicationExpression>(ImplicationExpression(SourceLocation(1,0), false_expression, false_expression));
    std::shared_ptr<ImplicationExpression> impl_expression_2 = std::make_shared<ImplicationExpression>(ImplicationExpression(SourceLocation(1,0), false_expression, true_expression));
    std::shared_ptr<ImplicationExpression> impl_expression_3 = std::make_shared<ImplicationExpression>(ImplicationExpression(SourceLocation(1,0), true_expression, false_expression));
    std::shared_ptr<ImplicationExpression> impl_expression_4 = std::make_shared<ImplicationExpression>(ImplicationExpression(SourceLocation(1,0), true_expression, true_expression));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(impl_expression_1);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(impl_expression_2);
    std::map<std::map<std::string, bool>, bool> result_3 = Evaluator::evaluateExpression(impl_expression_3);
    std::map<std::map<std::string, bool>, bool> result_4 = Evaluator::evaluateExpression(impl_expression_4);

    REQUIRE((result_1[std::map<std::string, bool>()]) == true);
    REQUIRE((result_2[std::map<std::string, bool>()]) == true);
    REQUIRE((result_3[std::map<std::string, bool>()]) == false);
    REQUIRE((result_4[std::map<std::string, bool>()]) == true);
}

TEST_CASE("Test biconditional expression evaluation", "[evaluator]")
{
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(1,0), false));

    std::shared_ptr<BiconditionalExpression> bicond_expression_1 = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(1,0), false_expression, false_expression));
    std::shared_ptr<BiconditionalExpression> bicond_expression_2 = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(1,0), false_expression, true_expression));
    std::shared_ptr<BiconditionalExpression> bicond_expression_3 = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(1,0), true_expression, false_expression));
    std::shared_ptr<BiconditionalExpression> bicond_expression_4 = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(1,0), true_expression, true_expression));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(bicond_expression_1);
    std::map<std::map<std::string, bool>, bool> result_2 = Evaluator::evaluateExpression(bicond_expression_2);
    std::map<std::map<std::string, bool>, bool> result_3 = Evaluator::evaluateExpression(bicond_expression_3);
    std::map<std::map<std::string, bool>, bool> result_4 = Evaluator::evaluateExpression(bicond_expression_4);

    REQUIRE((result_1[std::map<std::string, bool>()]) == true);
    REQUIRE((result_2[std::map<std::string, bool>()]) == false);
    REQUIRE((result_3[std::map<std::string, bool>()]) == false);
    REQUIRE((result_4[std::map<std::string, bool>()]) == true);
}


TEST_CASE("Test variable expression evaluation", "[evaluator]")
{
    std::shared_ptr<VariableExpression> var_expression = std::make_shared<VariableExpression>(VariableExpression(SourceLocation(1,0), "p"));

    std::map<std::map<std::string, bool>, bool> result_1 = Evaluator::evaluateExpression(var_expression);

    REQUIRE((result_1[std::map<std::string, bool>({{"p", true}})]) == true);
    REQUIRE((result_1[std::map<std::string, bool>({{"p", false}})]) == false);
}

TEST_CASE("Test evaluaton of unevaluable expression", "[evaluator]")
{
    REQUIRE_THROWS_MATCHES(Evaluator::evaluateExpressionFor(std::make_shared<Expression>(Expression()), std::map<std::string, bool>()), RuntimeErrorException, Catch::Matchers::ExceptionMessageMatcher("Encountered an unevaluable Expression Type!"));
}

TEST_CASE("Test evaluaton of expression with missing variable", "[evaluator]")
{
    REQUIRE_THROWS_MATCHES(Evaluator::evaluateExpressionFor(std::make_shared<VariableExpression>(VariableExpression()), std::map<std::string, bool>()), RuntimeErrorException, Catch::Matchers::ExceptionMessageMatcher("Tried evaluating a variable to which no mapping exists!"));
}
