/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <TruthTableCreator/exceptions/unable_to_cast_to_right_type_exception.hpp>
#include <TruthTableCreator/syntax/expression/expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>
#include <TruthTableCreator/syntax/expression/binary_expression.hpp>
#include <TruthTableCreator/syntax/expression/unary_expression.hpp>

TEST_CASE("Test default constructor", "[expression]")
{
  // Test Setup
  Expression expression_to_test = Expression();

  // Test
  REQUIRE(expression_to_test.getSourceLocation().equals(SourceLocation()));
  REQUIRE(expression_to_test.equals(std::make_shared<Expression>(Expression())));
}

TEST_CASE("Test main constructor", "[expression]")
{
  // Test Setup
  SourceLocation location = SourceLocation(1,2);
  Expression expression_to_test = Expression(location);

  // Test
  REQUIRE(expression_to_test.getSourceLocation() == SourceLocation(1,2));
  REQUIRE(expression_to_test.equals(std::make_shared<Expression>(Expression(SourceLocation(1,2)))));

  REQUIRE_FALSE(expression_to_test.getSourceLocation().equals(SourceLocation()));
  REQUIRE_FALSE(expression_to_test.equals(std::make_shared<Expression>(Expression(SourceLocation()))));
}

TEST_CASE("Test equals method exceptions", "[expression]")
{
  Expression constExpression = ConstExpression();
  Expression unaryExpression = UnaryExpression();
  Expression binaryExpression = BinaryExpression();

  REQUIRE_THROWS_MATCHES(constExpression.equals(std::make_shared<ConstExpression>(ConstExpression())), UnableToCastToRightExpressionTypeException, Catch::Matchers::ExceptionMessageMatcher("Could not cast the given expression to a ConstExpression!"));
  REQUIRE_THROWS_MATCHES(unaryExpression.equals(std::make_shared<UnaryExpression>(UnaryExpression())), UnableToCastToRightExpressionTypeException, Catch::Matchers::ExceptionMessageMatcher("Could not cast the given expression to a UnaryExpression!"));
  REQUIRE_THROWS_MATCHES(binaryExpression.equals(std::make_shared<BinaryExpression>(BinaryExpression())), UnableToCastToRightExpressionTypeException, Catch::Matchers::ExceptionMessageMatcher("Could not cast the given expression to a BinaryExpression!"));
}

TEST_CASE("Test << operator", "[expression]")
{
  // Test Setup
  SourceLocation location = SourceLocation(6,4);
  Expression expression = Expression(location);
  std::ostringstream stringstream;

  // Test
  stringstream << expression;
  REQUIRE(stringstream.str() == "Expression, Source Location: line: 6, column: 4");
}
