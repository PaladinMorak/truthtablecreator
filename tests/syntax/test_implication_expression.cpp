/**
 * @author Georg Bettenhausen
 * @date June 2023
 */
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <TruthTableCreator/syntax/expression/implication_expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>
#include <TruthTableCreator/syntax/expression/not_expression.hpp>

TEST_CASE("Test default constructor", "[implication_expression]")
{
  // Test Setup
  std::shared_ptr<ImplicationExpression> expression_to_test = std::make_shared<ImplicationExpression>(ImplicationExpression());

  // Test
  REQUIRE(expression_to_test -> getLeftHandOperand() -> equals(std::make_shared<Expression>(Expression())));
  REQUIRE(expression_to_test -> getRightHandOperand() -> equals(std::make_shared<Expression>(Expression())));
  REQUIRE(expression_to_test -> getSourceLocation().equals(SourceLocation()));
}

TEST_CASE("Test main constructor", "[implication_expression]")
{
  // Test Setup
  std::shared_ptr<ConstExpression> const_expression = std::make_shared<ConstExpression>(ConstExpression());
  std::shared_ptr<NotExpression> not_expression = std::make_shared<NotExpression>(NotExpression());
  SourceLocation location = SourceLocation(1,2);
  std::shared_ptr<ImplicationExpression> expression_to_test = std::make_shared<ImplicationExpression>(ImplicationExpression(location, const_expression, not_expression));

  // Test
  REQUIRE(expression_to_test -> getLeftHandOperand() -> equals(std::make_shared<ConstExpression>(ConstExpression())));
  REQUIRE(expression_to_test -> getRightHandOperand() -> equals(std::make_shared<NotExpression>(NotExpression())));
  REQUIRE(expression_to_test -> getSourceLocation().equals(location));
}

TEST_CASE("Test equals method", "[implication_expression]")
{
  // Test Setup
  SourceLocation location = SourceLocation(4,4);

  std::shared_ptr<ConstExpression> const_expression = std::make_shared<ConstExpression>(ConstExpression());
  std::shared_ptr<NotExpression> not_expression = std::make_shared<NotExpression>(NotExpression());
  std::shared_ptr<ImplicationExpression> expression_to_test = std::make_shared<ImplicationExpression>(ImplicationExpression(location, const_expression, not_expression));

  // Test Equals
  REQUIRE(expression_to_test -> equals(std::make_shared<ImplicationExpression>(ImplicationExpression(location, std::make_shared<ConstExpression>(ConstExpression()), std::make_shared<NotExpression>(NotExpression())))));

  // Test Not Equals
  REQUIRE_FALSE(expression_to_test -> equals(std::make_shared<UnaryExpression>(UnaryExpression())));
  REQUIRE_FALSE(expression_to_test -> equals(std::make_shared<ImplicationExpression>(ImplicationExpression())));
  REQUIRE_FALSE(expression_to_test -> equals(std::make_shared<BinaryExpression>(BinaryExpression(location, std::make_shared<ConstExpression>(ConstExpression()), std::make_shared<NotExpression>(NotExpression())))));
}

TEST_CASE("Test std::string operator", "[implication_expression]")
{
    // Test Setup
    SourceLocation location = SourceLocation(4,5);
    std::shared_ptr<ConstExpression> const_expression = std::make_shared<ConstExpression>(ConstExpression());
    std::shared_ptr<NotExpression> not_expression = std::make_shared<NotExpression>(NotExpression());
    std::shared_ptr<ImplicationExpression> expression_to_test = std::make_shared<ImplicationExpression>(ImplicationExpression(location, const_expression, not_expression));

    // Test
    REQUIRE(std::string(*expression_to_test) == "ImplicationExpression, Source Location: " + std::string(location) + "; Left-Hand Operand: " + std::string(*(expression_to_test -> getLeftHandOperand())) + "; Right-Hand Operand: " + std::string(*(expression_to_test -> getRightHandOperand())));
}

TEST_CASE("Test toString method", "[implication_expression]")
{
    // Test Setup
    SourceLocation location = SourceLocation(4,5);
    std::shared_ptr<ConstExpression> const_expression = std::make_shared<ConstExpression>(ConstExpression());
    std::shared_ptr<NotExpression> not_expression = std::make_shared<NotExpression>(NotExpression());
    std::shared_ptr<ImplicationExpression> expression_to_test = std::make_shared<ImplicationExpression>(ImplicationExpression(location, const_expression, not_expression));

    // Test
    REQUIRE(expression_to_test -> toString() == "ImplicationExpression, Source Location: " + std::string(location) + "; Left-Hand Operand: " + std::string(*(expression_to_test -> getLeftHandOperand())) + "; Right-Hand Operand: " + std::string(*(expression_to_test -> getRightHandOperand())));
}
