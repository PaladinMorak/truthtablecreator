/**
 * @author Georg Bettenhausen
 * @date January 2024
 */
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <TruthTableCreator/syntax/node_type.hpp>

TEST_CASE("Test to string method", "[node_type]")
{
  // Test
  REQUIRE(NodeType::typeToString(NodeType::Node) == "Node");
  REQUIRE(NodeType::typeToString(NodeType::Expression) == "Expression");
  REQUIRE(NodeType::typeToString(NodeType::BinaryExpression) == "BinaryExpression");
  REQUIRE(NodeType::typeToString(NodeType::UnaryExpression) == "UnaryExpression");
  REQUIRE(NodeType::typeToString(NodeType::ConstExpression) == "ConstExpression");
}
