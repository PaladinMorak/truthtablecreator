/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <ostream>
#include <iostream>
#include <sstream>

#include <TruthTableCreator/syntax/source_location.hpp>


TEST_CASE("Test default constructor", "[source_location]")
{
  SourceLocation location = SourceLocation();
  REQUIRE(location.getLine() == 0);
  REQUIRE(location.getColumn() == 0);
}

TEST_CASE("Test specific constructor", "[source_location]")
{
  SourceLocation location = SourceLocation(20, 5);
  REQUIRE(location.getLine() == 20);
  REQUIRE(location.getColumn() == 5);
}

TEST_CASE("Test to string method", "[source_location]")
{
  SourceLocation location = SourceLocation(1234,5678);
  REQUIRE(std::string(location) == "line: 1234, column: 5678");
}

TEST_CASE("Test equals method", "[source_location]")
{
  SourceLocation loc1 = SourceLocation(1,1);
  SourceLocation loc2 = SourceLocation(1,1);
  SourceLocation loc3 = SourceLocation(1,3);

  REQUIRE(loc1 == loc2);
  REQUIRE_FALSE(loc1.equals(loc3));
}

TEST_CASE("Test << operator", "[source_location]")
{
  // Test Setup
  SourceLocation location = SourceLocation(9,3);
  std::ostringstream stringstream;

  // Test
  stringstream << location;
  REQUIRE(stringstream.str() == "line: 9, column: 3");
}
