/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <iostream>

#include <TruthTableCreator/syntax/expression/variable_expression.hpp>

TEST_CASE("Test Default Constructor", "[variable]")
{
    // Test Setup
    std::shared_ptr<SourceLocation> location = std::make_shared<SourceLocation>(SourceLocation());
    std::shared_ptr<VariableExpression> variable_to_check = std::make_shared<VariableExpression>(VariableExpression());

    // Test that default constructor initializes members correctly
    REQUIRE(variable_to_check -> getSourceLocation().equals(*location));
    REQUIRE(variable_to_check -> getSpelling() == "");

    // Test that variable is not equal to AstNode
    REQUIRE_FALSE(variable_to_check -> equals(std::make_shared<AstNode>(AstNode())));
}

TEST_CASE("Test Main Constructor", "[variable]")
{
    // Test Setup
    SourceLocation location = SourceLocation(8, 9);
    std::string spelling = "q";
    std::shared_ptr<VariableExpression> variable_to_check = std::make_shared<VariableExpression>(VariableExpression(location, spelling));

    // Test that main constructor initializes members correctly
    REQUIRE(variable_to_check -> getSourceLocation() == location);
    REQUIRE(variable_to_check -> getSpelling() == spelling);
}

TEST_CASE("Test equals method", "[variable]")
{
    // Test Setup
    SourceLocation location1 = SourceLocation(8, 9);
    SourceLocation location2 = SourceLocation(4, 5);

    std::string spelling1 = "q";
    std::string spelling2 = "p";

    std::shared_ptr<VariableExpression> variable1 = std::make_shared<VariableExpression>(VariableExpression(location1, spelling1));
    std::shared_ptr<VariableExpression> variable2 = std::make_shared<VariableExpression>(VariableExpression(location2, spelling2));

    // Test Equals
    REQUIRE(variable1 -> equals(std::make_shared<VariableExpression>(VariableExpression(location1, spelling1))));
    REQUIRE(variable1 -> equals(std::make_shared<VariableExpression>(VariableExpression(location2, spelling1))));
    REQUIRE(variable2 -> equals(std::make_shared<VariableExpression>(VariableExpression(location1, spelling2))));
    REQUIRE(variable2 -> equals(std::make_shared<VariableExpression>(VariableExpression(location2, spelling2))));

    // Test Not Equals
    REQUIRE_FALSE(variable1 -> equals(std::make_shared<VariableExpression>(VariableExpression(location1, spelling2))));
    REQUIRE_FALSE(variable1 -> equals(std::make_shared<VariableExpression>(VariableExpression(location2, spelling2))));
    REQUIRE_FALSE(variable2 -> equals(std::make_shared<VariableExpression>(VariableExpression(location1, spelling1))));
    REQUIRE_FALSE(variable2 -> equals(std::make_shared<VariableExpression>(VariableExpression(location2, spelling1))));
}

TEST_CASE("Test std::string operator", "[variable]")
{
    // Test Setup
    SourceLocation source_location = SourceLocation(3,4);
    std::string spelling = "asdf";
    std::shared_ptr<VariableExpression> variable = std::make_shared<VariableExpression>(VariableExpression(source_location, spelling));

    REQUIRE(std::string(*variable) == "VariableExpression, Source Location: " + std::string(source_location) + "; Spelling: " + spelling);
}

TEST_CASE("Test toSring method", "[variable]")
{
    // Test Setup
    SourceLocation source_location = SourceLocation(3,4);
    std::string spelling = "asdf";
    std::shared_ptr<VariableExpression> variable = std::make_shared<VariableExpression>(VariableExpression(source_location, spelling));

    REQUIRE(variable -> toString() == "VariableExpression, Source Location: " + std::string(source_location) + "; Spelling: " + spelling);
}
