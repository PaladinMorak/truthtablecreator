/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <TruthTableCreator/syntax/ast_node.hpp>

TEST_CASE("Test default constructor", "[ast_node]")
{
  // Test Setup
  AstNode node = AstNode();

  // Test
  REQUIRE(node.getSourceLocation().equals(SourceLocation()));
  REQUIRE(node.equals(std::make_shared<AstNode>(AstNode())));
}

TEST_CASE("Test main constructor", "[ast_node]")
{
  // Test Setup
  SourceLocation location = SourceLocation(6,4);
  AstNode node = AstNode(location);

  // Test
  REQUIRE(node.getSourceLocation().equals(SourceLocation(6,4)));
  REQUIRE(node.equals(std::make_shared<AstNode>(AstNode(SourceLocation(6,4)))));

  REQUIRE_FALSE(node.getSourceLocation().equals(SourceLocation()));
  REQUIRE_FALSE(node.equals(std::make_shared<AstNode>(AstNode())));
}

TEST_CASE("Test toString method", "[ast_node]")
{
  // Test Setup
  SourceLocation location = SourceLocation(6,4);
  AstNode node = AstNode(location);

  // Test
  REQUIRE(node.toString() == "AstNode, Source Location: line: 6, column: 4");
}

TEST_CASE("Test std::string method", "[ast_node]")
{
  // Test Setup
  SourceLocation location = SourceLocation(6,4);
  AstNode node = AstNode(location);

  // Test
  REQUIRE(std::string(node) == "AstNode, Source Location: line: 6, column: 4");
}

TEST_CASE("Test << operator", "[ast_node]")
{
  // Test Setup
  SourceLocation location = SourceLocation(6,4);
  AstNode node = AstNode(location);
  std::ostringstream stringstream;

  // Test
  stringstream << node;
  REQUIRE(stringstream.str() == "AstNode, Source Location: line: 6, column: 4");
}
