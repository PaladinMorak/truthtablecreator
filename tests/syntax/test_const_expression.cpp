/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <TruthTableCreator/syntax/expression/const_expression.hpp>

TEST_CASE("Test default constructor", "[const_expression]")
{
  // Test Setup
  ConstExpression expression_to_test = ConstExpression();

  // Test
  REQUIRE(expression_to_test.equals(std::make_shared<ConstExpression>(ConstExpression())));
  REQUIRE(expression_to_test.getValue() == false);
  REQUIRE(expression_to_test.getSourceLocation().equals(SourceLocation()));
}

TEST_CASE("Test main constructor", "[const_expression]")
{
  // Test Setup
  SourceLocation location = SourceLocation(7,9);
  bool value = true;
  ConstExpression expression_to_test = ConstExpression(location, value);

  // Test
  REQUIRE(expression_to_test.equals(std::make_shared<ConstExpression>(ConstExpression(location, value))));
  REQUIRE(expression_to_test.getValue() == true);
  REQUIRE(expression_to_test.getSourceLocation() == location);
}

TEST_CASE("Test equals method", "[const_expression]")
{
  SourceLocation loc1 = SourceLocation(1,1);
  SourceLocation loc2 = SourceLocation(2,2);

  std::shared_ptr<ConstExpression> expr1_true = std::make_shared<ConstExpression>(ConstExpression(loc1, true));
  std::shared_ptr<ConstExpression> expr1_false = std::make_shared<ConstExpression>(ConstExpression(loc1, false));
  std::shared_ptr<ConstExpression> expr2_true = std::make_shared<ConstExpression>(ConstExpression(loc2, true));
  std::shared_ptr<ConstExpression> expr2_false = std::make_shared<ConstExpression>(ConstExpression(loc2, false));

  // These should all be equal since they have the same value
  REQUIRE(expr1_true -> equals(expr1_true));
  REQUIRE(expr1_true -> equals(expr2_true));
  REQUIRE(expr1_false -> equals(expr1_false));
  REQUIRE(expr1_false -> equals(expr2_false));

  REQUIRE(expr2_true -> equals(expr2_true));
  REQUIRE(expr2_true -> equals(expr1_true));
  REQUIRE(expr2_false -> equals(expr2_false));
  REQUIRE(expr2_false -> equals(expr1_false));

  // These should not be equal since they do not have the same value
  REQUIRE_FALSE(expr1_true -> equals(expr1_false));
  REQUIRE_FALSE(expr1_true -> equals(expr2_false));
  REQUIRE_FALSE(expr1_false -> equals(expr1_true));
  REQUIRE_FALSE(expr1_false -> equals(expr2_true));

  REQUIRE_FALSE(expr2_true -> equals(expr2_false));
  REQUIRE_FALSE(expr2_true -> equals(expr1_false));
  REQUIRE_FALSE(expr2_false -> equals(expr2_true));
  REQUIRE_FALSE(expr2_false -> equals(expr1_true));
}

TEST_CASE("Test std::string method","[const_expression]")
{
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<ConstExpression> expression_to_test = std::make_shared<ConstExpression>(ConstExpression(operator_location, false));

    REQUIRE(std::string(*expression_to_test) == "ConstExpression, Source Location: " + std::string(operator_location) + "; Value: false");
}

TEST_CASE("Test toString method","[const_expression]")
{
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<ConstExpression> expression_to_test = std::make_shared<ConstExpression>(ConstExpression(operator_location, false));

    REQUIRE(expression_to_test -> toString() == "ConstExpression, Source Location: " + std::string(operator_location) + "; Value: false");
}
