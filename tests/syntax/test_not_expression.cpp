/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <iostream>

#include <TruthTableCreator/syntax/expression/not_expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>


TEST_CASE("Test default constructor", "[not_expression]")
{
  // Test Setup
  NotExpression expression_to_test = NotExpression();

  // Test
  REQUIRE(expression_to_test.getSourceLocation().equals(SourceLocation()));
  REQUIRE(expression_to_test.getOperand() -> equals(std::make_shared<Expression>(Expression())));
}

TEST_CASE("Test main constructor", "[not_expression]")
{
  // Test Setup False Const Expression
  SourceLocation operator_location = SourceLocation(5,10);
  SourceLocation operand_location = SourceLocation(5,11);
  std::shared_ptr<Expression> operand_false = std::make_shared<ConstExpression>(ConstExpression(operand_location, false));
  std::shared_ptr<NotExpression> expression_to_test_false = std::make_shared<NotExpression>(NotExpression(operator_location, operand_false));

  // Test False Const Expression
  REQUIRE(expression_to_test_false->getSourceLocation() == operator_location);
  REQUIRE(expression_to_test_false->getOperand() -> equals(operand_false));
  REQUIRE(expression_to_test_false->getOperand() -> equals(std::make_shared<ConstExpression>(ConstExpression())));

  REQUIRE_FALSE(expression_to_test_false->getSourceLocation().equals(SourceLocation()));
  REQUIRE_FALSE(expression_to_test_false->getOperand() -> equals(std::make_shared<ConstExpression>(ConstExpression(SourceLocation(5,11), true))));

  // Test Setup True Const Expression
  std::shared_ptr<Expression> operand_true = std::make_shared<ConstExpression>(ConstExpression(operand_location, true));
  NotExpression expression_to_test_true = NotExpression(operator_location, operand_true);

  // Test True Const Expression
  REQUIRE(expression_to_test_true.getSourceLocation() == operator_location);
  REQUIRE(expression_to_test_true.getOperand() -> equals(operand_true));

  REQUIRE_FALSE(expression_to_test_true.getSourceLocation().equals(SourceLocation()));
  REQUIRE_FALSE(expression_to_test_true.getOperand() -> equals(std::make_shared<ConstExpression>(ConstExpression())));
  REQUIRE_FALSE(expression_to_test_true.getOperand() -> equals(std::make_shared<ConstExpression>(ConstExpression(SourceLocation(5,11), false))));
}

TEST_CASE("Test equals method", "[not_expression]")
{
    std::shared_ptr<NotExpression> notExpression0 = std::make_shared<NotExpression>(NotExpression());
    std::shared_ptr<NotExpression> notExpression1 = std::make_shared<NotExpression>(NotExpression(SourceLocation(2,3), std::make_shared<ConstExpression>(ConstExpression(SourceLocation(2,4), true))));
    std::shared_ptr<NotExpression> notExpression2 = std::make_shared<NotExpression>(NotExpression(SourceLocation(2,3), std::make_shared<ConstExpression>(ConstExpression(SourceLocation(2,4), true))));
    std::shared_ptr<UnaryExpression> unaryExpression = std::make_shared<UnaryExpression>(UnaryExpression(SourceLocation(2,3), std::make_shared<ConstExpression>(ConstExpression(SourceLocation(2,4), true))));

  // Not Equal
  REQUIRE(notExpression0 -> equals(notExpression0));
  REQUIRE(notExpression1 -> equals(notExpression1));
  REQUIRE(notExpression1 -> equals(notExpression2));
  REQUIRE(notExpression2 -> equals(notExpression1));
  REQUIRE(notExpression2 -> equals(notExpression2));

  // Not Equal
  REQUIRE_FALSE(notExpression0 -> equals(notExpression1));
  REQUIRE_FALSE(notExpression0 -> equals(notExpression2));
  REQUIRE_FALSE(notExpression1 -> equals(notExpression0));
  REQUIRE_FALSE(notExpression2 -> equals(notExpression0));
  REQUIRE_FALSE(notExpression0 -> equals(std::make_shared<UnaryExpression>(UnaryExpression())));
  REQUIRE_FALSE(notExpression1 -> equals(unaryExpression));
}

TEST_CASE("Test std::string operator", "[not_expression]")
{
    SourceLocation operand_location = SourceLocation(1234,9876);
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<NotExpression> expression_to_test = std::make_shared<NotExpression>(NotExpression(operator_location, operand));

    REQUIRE(std::string(*expression_to_test) == "NotExpression, Source Location: " + std::string(operator_location) + "; Operand: " + std::string(*operand));
}

TEST_CASE("Test to string method", "[not_expression]")
{
    SourceLocation operand_location = SourceLocation(1234,9876);
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<NotExpression> expression_to_test = std::make_shared<NotExpression>(NotExpression(operator_location, operand));

    REQUIRE(expression_to_test -> toString() == "NotExpression, Source Location: " + std::string(operator_location) + "; Operand: " + std::string(*operand));
}
TEST_CASE("Test << operator", "[not_expression]")
{
    SourceLocation operand_location = SourceLocation(1234,9876);
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<NotExpression> expression_to_test = std::make_shared<NotExpression>(NotExpression(operator_location, operand));

  // Test
    std::ostringstream stringstream;
  stringstream << *expression_to_test;
    REQUIRE(stringstream.str() == "NotExpression, Source Location: " + std::string(operator_location) + "; Operand: " + std::string(*operand));
}
