/**
 * @author Georg Bettenhausen
 * @date January 2023
 */

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>

#include <iostream>

#include <TruthTableCreator/syntax/expression/unary_expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>

TEST_CASE("Test default constructor", "[unary_expression]")
{
    // Test Setup
    std::shared_ptr<UnaryExpression> expression_to_test = std::make_shared<UnaryExpression>(UnaryExpression());

    // Test
    REQUIRE(expression_to_test -> getSourceLocation().equals(SourceLocation()));
    REQUIRE(expression_to_test -> getOperand() -> equals(std::make_shared<Expression>(Expression())));
}

TEST_CASE("Test main constructor", "[unary_expression]")
{
    // Test SEtup
    SourceLocation operator_location = SourceLocation(3,10);
    SourceLocation operand_location = SourceLocation(3,11);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<UnaryExpression> expression_to_test = std::make_shared<UnaryExpression>(UnaryExpression(operator_location, operand));

    REQUIRE(expression_to_test -> getSourceLocation() == operator_location);
    REQUIRE(expression_to_test -> getOperand() -> equals(operand));

    REQUIRE_FALSE(expression_to_test -> getSourceLocation() == SourceLocation());
    REQUIRE_FALSE(expression_to_test -> getOperand() -> equals(std::make_shared<Expression>(Expression())));
}

TEST_CASE("Test equals method", "[unary_expression]")
{
    // Test Setup
    SourceLocation operator1_location = SourceLocation(1,2);
    SourceLocation operator2_location = SourceLocation(2,3);
    SourceLocation operator3_location = SourceLocation(1,2);
    SourceLocation operand1_location = SourceLocation(1,3);
    SourceLocation operand2_location = SourceLocation(2,4);
    SourceLocation operand3_location = SourceLocation(1,3);

    std::shared_ptr<Expression> operand1 = std::make_shared<Expression>(Expression(operand1_location));
    std::shared_ptr<Expression> operand2 = std::make_shared<Expression>(Expression(operand2_location));
    std::shared_ptr<Expression> operand3 = std::make_shared<Expression>(Expression(operand3_location));

    std::shared_ptr<Expression> operator1 = std::make_shared<UnaryExpression>(UnaryExpression(operator1_location, operand1));
    std::shared_ptr<Expression> operator2 = std::make_shared<UnaryExpression>(UnaryExpression(operator2_location, operand2));
    std::shared_ptr<Expression> operator3 = std::make_shared<UnaryExpression>(UnaryExpression(operator3_location, operand3));
    Expression operator4 = UnaryExpression(operator3_location, operand3);

    // Test True
    REQUIRE(operator1 -> equals(operator3));
    REQUIRE(operator3 -> equals(operator1));

    // Test False
    REQUIRE_FALSE(operator1 -> equals(operator2));
    REQUIRE_FALSE(operator2 -> equals(operator1));
    REQUIRE_FALSE(operator2 -> equals(operator3));
    REQUIRE_FALSE(operator3 -> equals(operator2));

    // Test cannot cast
    REQUIRE_FALSE(operator3 -> equals(std::make_shared<Expression>(operator4)));
}

TEST_CASE("Test std::string operator", "[unary_expression]")
{
    SourceLocation operand_location = SourceLocation(1234,9876);
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<UnaryExpression> expression_to_test = std::make_shared<UnaryExpression>(UnaryExpression(operator_location, operand));

    REQUIRE(std::string(*expression_to_test) == "UnaryExpression, Source Location: " + std::string(operator_location) + "; Operand: " + std::string(*operand));
}

TEST_CASE("Test to string method", "[unary_expression]")
{
    SourceLocation operand_location = SourceLocation(1234,9876);
    SourceLocation operator_location = SourceLocation(4321,6789);

    std::shared_ptr<Expression> operand = std::make_shared<Expression>(Expression(operand_location));
    std::shared_ptr<UnaryExpression> expression_to_test = std::make_shared<UnaryExpression>(UnaryExpression(operator_location, operand));

    REQUIRE(expression_to_test -> toString() == "UnaryExpression, Source Location: " + std::string(operator_location) + "; Operand: " + std::string(*operand));
}
