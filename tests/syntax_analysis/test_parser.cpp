#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>
#include <istream>
#include <deque>

#include <TruthTableCreator/syntax_analysis/token.hpp>
#include <TruthTableCreator/syntax_analysis/lexer.hpp>
#include <TruthTableCreator/syntax_analysis/parser.hpp>

TEST_CASE("Test Complete File That Works", "[parser]")
{
    // Control Expression ((p -> 1) & (p | (1 <-> !0)))
    std::shared_ptr<ConstExpression> true_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(), true));
    std::shared_ptr<ConstExpression> false_expression = std::make_shared<ConstExpression>(ConstExpression(SourceLocation(), false));
    std::shared_ptr<VariableExpression> var_expression = std::make_shared<VariableExpression>(VariableExpression(SourceLocation(), "p"));
    std::shared_ptr<ImplicationExpression> impl_expression = std::make_shared<ImplicationExpression>(ImplicationExpression(SourceLocation(), var_expression, true_expression));
    std::shared_ptr<NotExpression> not_expression = std::make_shared<NotExpression>(NotExpression(SourceLocation(), false_expression));
    std::shared_ptr<BiconditionalExpression> bic_expression = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(), true_expression, not_expression));
    std::shared_ptr<OrExpression> or_expression = std::make_shared<OrExpression>(OrExpression(SourceLocation(), var_expression, bic_expression));
    std::shared_ptr<AndExpression> and_expression = std::make_shared<AndExpression>(AndExpression(SourceLocation(), impl_expression, or_expression));

    std::shared_ptr<BiconditionalExpression> bic_expression2 = std::make_shared<BiconditionalExpression>(BiconditionalExpression(SourceLocation(), not_expression, true_expression));


    std::ifstream basicInputFile ("tests/syntax_analysis/basic_parser_test.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);
    std::list<std::shared_ptr<Expression>> expressions = par.parse();

    REQUIRE(expressions.front() -> equals(and_expression));

    expressions.pop_front();

    REQUIRE(expressions.front() -> equals(bic_expression2));

    expressions.pop_front();

    REQUIRE(expressions.front() -> equals(bic_expression2));
}

TEST_CASE("Test missing left hand operand for or expression", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/or_missing_left_hand.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No left-hand operand provided for OR expression at: line: 2, column: 0"));
}

TEST_CASE("Test missing left hand operand for and expression", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/and_missing_left_hand.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No left-hand operand provided for AND expression at: line: 2, column: 0"));
}

TEST_CASE("Test missing left hand operand for impl expression", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/impl_missing_left_hand.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No left-hand operand provided for IMPLICATION expression at: line: 2, column: 0"));
}

TEST_CASE("Test missing left hand operand for biconditionas expression", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/bic_missing_left_hand.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No left-hand operand provided for BICONDITIONAL expression at: line: 2, column: 0"));
}

TEST_CASE("Test multiple expressions in one line", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/multiple_expressions.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: Found more than one expression in line: 2"));
}

TEST_CASE("Test floating right paranthese", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/floating_right_para.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: Unexpected closing paranthese at: line: 2, column: 8"));
}

TEST_CASE("Test floating START token", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/floating_START.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: Unexpected START command at: line: 2, column: 0"));
}

TEST_CASE("Test unexpected EOL")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/unexpected_End.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: Unexpected END command at: line: 2, column: 5"));
}

TEST_CASE("Test RuntimeError")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/unexpected_End.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = std::deque<Token>();
    tokens.push_back(Token(TokenType::START, "", 0,0));

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), RuntimeErrorException, Catch::Matchers::ExceptionMessageMatcher("Encountered an empty token list during parsing!"));
}

TEST_CASE("Test Empty Parantheses", "[parser]")
{
    std::ifstream basicInputFile ("tests/syntax_analysis/empty_parantheses.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> tokens = lex.scan();

    Parser par = Parser(tokens);

    REQUIRE_THROWS_MATCHES(par.parse(), SyntaxErrorException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: Found an empty paranthesis at: line: 2, column: 2"));
}
