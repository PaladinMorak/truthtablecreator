#include <catch2/catch_test_macros.hpp>
#include <ostream>
#include <iostream>
#include <sstream>

#include <TruthTableCreator/syntax_analysis/token.hpp>
#include <TruthTableCreator/syntax_analysis/token_type.hpp>
#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>

TEST_CASE( "Different token type", "[token]" )
{
    Token tok1 = Token(TokenType::VAR, "asdf", 1, 0);
    Token tok2 = Token(TokenType::AND, "&", 1, 0);
    REQUIRE(!(tok1.equals(tok2)));
    REQUIRE(!(tok1 == tok2));
}

TEST_CASE( "Different token spelling", "[token]" )
{
    Token tok1 = Token(TokenType::VAR, "asdf", 1, 0);
    Token tok2 = Token(TokenType::VAR, "asdf2", 1, 0);
    REQUIRE(!(tok1.equals(tok2)));
    REQUIRE(!(tok1 == tok2));
}

TEST_CASE( "Different token line", "[token]" )
{
    Token tok1 = Token(TokenType::VAR, "asdf", 1, 0);
    Token tok2 = Token(TokenType::VAR, "asdf", 2, 0);
    REQUIRE(!(tok1.equals(tok2)));
    REQUIRE(!(tok1 == tok2));
}
TEST_CASE( "Different token column", "[token]" )
{
    Token tok1 = Token(TokenType::VAR, "asdf", 1, 0);
    Token tok2 = Token(TokenType::VAR, "asdf", 1, 1);
    REQUIRE(!(tok1.equals(tok2)));
    REQUIRE(!(tok1 == tok2));
}

TEST_CASE( "Equal tokens", "[token]" )
{
    Token tok1 = Token(TokenType::VAR, "asdf", 1, 0);
    Token tok2 = Token(TokenType::VAR, "asdf", 1, 0);
    REQUIRE(tok1.equals(tok2));
    REQUIRE(tok1 == tok2);
}

TEST_CASE( "String method", "[token]" )
{
    // Required Tokens
    Token var_token = Token(TokenType::VAR, "asdf", 1, 0);
    Token true_token = Token(TokenType::TRUE, "true", 5, 11);
    Token false_token = Token(TokenType::FALSE, "false", 0, 23);
    Token not_token = Token(TokenType::NOT, "!", 3, 6);
    Token and_token = Token(TokenType::AND, "&", 2, 9);
    Token or_token = Token(TokenType::OR, "|", 9, 13);
    Token impl_token = Token(TokenType::IMPL, "->", 5, 909);
    Token bicond_token = Token(TokenType::BICOND, "<->", 2, 96);
    Token lparen_token = Token(TokenType::LPAREN, "(", 53, 12);
    Token rparen_token = Token(TokenType::RPAREN, ")", 3, 2);
    Token start_token = Token(TokenType::START, "<START>", 2, 3);
    Token end_token = Token(TokenType::END, "<END>", 23, 32);
    Token eol_token = Token(TokenType::EOL, "\n", 2, 5);

    // Tokens as expected strings
    std::string var_token_as_string = "VAR: asdf l: 1 c: 0";
    std::string true_token_as_string = "TRUE: true l: 5 c: 11";
    std::string false_token_as_string = "FALSE: false l: 0 c: 23";
    std::string not_token_as_string = "NOT: ! l: 3 c: 6";
    std::string and_token_as_string = "AND: & l: 2 c: 9";
    std::string or_token_as_string = "OR: | l: 9 c: 13";
    std::string impl_token_as_string = "IMPL: -> l: 5 c: 909";
    std::string bicond_token_as_string = "BICOND: <-> l: 2 c: 96";
    std::string lparen_token_as_string = "LPAREN: ( l: 53 c: 12";
    std::string rparen_token_as_string = "RPAREN: ) l: 3 c: 2";
    std::string start_token_as_string = "START: <START> l: 2 c: 3";
    std::string end_token_as_string = "END: <END> l: 23 c: 32";
    std::string eol_token_as_string = "EOL: \\n l: 2 c: 5";

    REQUIRE(std::string(var_token) == var_token_as_string);
    REQUIRE(std::string(true_token) == true_token_as_string );
    REQUIRE(std::string(false_token) == false_token_as_string);
    REQUIRE(std::string(not_token) == not_token_as_string);
    REQUIRE(std::string(and_token) == and_token_as_string);
    REQUIRE(std::string(or_token) == or_token_as_string);
    REQUIRE(std::string(impl_token) == impl_token_as_string);
    REQUIRE(std::string(bicond_token) == bicond_token_as_string);
    REQUIRE(std::string(lparen_token) == lparen_token_as_string);
    REQUIRE(std::string(rparen_token) == rparen_token_as_string);
    REQUIRE(std::string(start_token) == start_token_as_string);
    REQUIRE(std::string(end_token) == end_token_as_string);
    REQUIRE(std::string(eol_token) == eol_token_as_string);
}

TEST_CASE( "ostream operator", "[token]" )
{
    // Test setup
    std::ostringstream os;
    Token var_token = Token(TokenType::VAR, "asdf", 1, 0);
    std::string var_token_as_string = "VAR: asdf l: 1 c: 0";

    // Test
    os << var_token;
    REQUIRE(os.str() == var_token_as_string);
}
