#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_exception.hpp>
#include <istream>
#include <deque>

#include <TruthTableCreator/syntax_analysis/lexer.hpp>
#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>
#include <TruthTableCreator/exceptions/unexpected_eof_exception.hpp>
#include <TruthTableCreator/exceptions/file_error_exception.hpp>

TEST_CASE("Test Complete File That Works", "[lexer]")
{
    // Create the correct list of tokens
    std::deque<Token> correctTokens = std::deque<Token>();
    correctTokens.push_back(Token(TokenType::START, "<START>", 1, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 1, 7));
    correctTokens.push_back(Token(TokenType::VAR, "q", 2, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 2, 1));
    correctTokens.push_back(Token(TokenType::TRUE, "1", 3, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 3, 1));
    correctTokens.push_back(Token(TokenType::FALSE, "0", 4, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 4, 1));
    correctTokens.push_back(Token(TokenType::NOT, "!", 5, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 5, 1));
    correctTokens.push_back(Token(TokenType::AND, "&", 6, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 6, 1));
    correctTokens.push_back(Token(TokenType::OR, "|", 7, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 7, 1));
    correctTokens.push_back(Token(TokenType::IMPL, "->", 8, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 8, 2));
    correctTokens.push_back(Token(TokenType::BICOND, "<->", 9, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 9, 3));
    correctTokens.push_back(Token(TokenType::LPAREN, "(", 10, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 10, 1));
    correctTokens.push_back(Token(TokenType::RPAREN, ")", 11, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 11, 1));
    correctTokens.push_back(Token(TokenType::END, "<END>", 12, 0));
    correctTokens.push_back(Token(TokenType::EOL, "\n", 12, 5));

    std::ifstream basicInputFile ("tests/syntax_analysis/basic_test.txt");
    Lexer lex = Lexer(&basicInputFile);
    std::deque<Token> actualTokens = lex.scan();

    REQUIRE(actualTokens == correctTokens);
}

TEST_CASE("Test Broken Start", "[lexer]")
{
    std::ifstream brokenStartFile ("tests/syntax_analysis/broken_start_test.txt");
    Lexer lex = Lexer(&brokenStartFile);
    REQUIRE_THROWS_MATCHES(lex.scan(), NoMatchingTokenTypeException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No Matching Token exists for the spelling \"<STA\" started at line: 1 and column: 0"));
}

TEST_CASE("Test Broken End", "[lexer]")
{
    std::ifstream brokenEndFile ("tests/syntax_analysis/broken_end_test.txt");
    Lexer lex = Lexer(&brokenEndFile);
    REQUIRE_THROWS_MATCHES(lex.scan(), NoMatchingTokenTypeException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No Matching Token exists for the spelling \"<END\" started at line: 3 and column: 0"));
}

TEST_CASE("Test Broken Impication", "[lexer]")
{
    std::ifstream brokenImplicationFile ("tests/syntax_analysis/broken_implication_test.txt");
    Lexer lex = Lexer(&brokenImplicationFile);
    REQUIRE_THROWS_MATCHES(lex.scan(), NoMatchingTokenTypeException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No Matching Token exists for the spelling \"-\" started at line: 2 and column: 0"));
}

TEST_CASE("Test Broken Biconditional", "[lexer]")
{
    std::ifstream brokenBiconditionalFile ("tests/syntax_analysis/broken_biconditional_test.txt");
    Lexer lex = Lexer(&brokenBiconditionalFile);
    REQUIRE_THROWS_MATCHES(lex.scan(), NoMatchingTokenTypeException, Catch::Matchers::ExceptionMessageMatcher("Syntax Error: No Matching Token exists for the spelling \"<-\" started at line: 2 and column: 0"));
}

TEST_CASE("Test File doesn't exist", "[lexer]")
{
    std::ifstream unexistingFile ("tests/syntax_analysis/unexisting_file.txt");
    REQUIRE_THROWS_MATCHES(Lexer(&unexistingFile), FileErrorException, Catch::Matchers::ExceptionMessageMatcher("Encountered a problem while trying to open the file!"));
}
