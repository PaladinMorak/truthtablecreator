#include <catch2/catch_test_macros.hpp>

#include <TruthTableCreator/syntax_analysis/token_type.hpp>
#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>

TEST_CASE( "ID token type", "[token_type]" )
{
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("asdf"));
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("ASDF"));
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("aSdF"));
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("AsDf"));
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("q1"));
  REQUIRE(TokenType::VAR == TokenType::spellingToTokenType("Q1234567890Q"));
}

TEST_CASE("TRUE token type", "[token_type]")
{
  REQUIRE(TokenType::TRUE == TokenType::spellingToTokenType("1"));
}

TEST_CASE("FALSE token type", "[token_type]")
{
  REQUIRE(TokenType::FALSE == TokenType::spellingToTokenType("0"));
}

TEST_CASE("NOT token type", "[token_type]")
{
  REQUIRE(TokenType::NOT == TokenType::spellingToTokenType("!"));
}

TEST_CASE("AND token type", "[token_type]")
{
  REQUIRE(TokenType::AND == TokenType::spellingToTokenType("&"));
}

TEST_CASE("OR token type", "[token_type]")
{
  REQUIRE(TokenType::OR == TokenType::spellingToTokenType("|"));
}

TEST_CASE("IMPL token type", "[token_type]")
{
  REQUIRE(TokenType::IMPL == TokenType::spellingToTokenType("->"));
}

TEST_CASE("BIIMPML token type", "[token_type]")
{
  REQUIRE(TokenType::BICOND == TokenType::spellingToTokenType("<->"));
}

TEST_CASE("LPAREN token type", "[token_type]")
{
  REQUIRE(TokenType::LPAREN == TokenType::spellingToTokenType("("));
}

TEST_CASE("RPAREN token type", "[token_type]")
{
  REQUIRE(TokenType::RPAREN == TokenType::spellingToTokenType(")"));
}

TEST_CASE("START token type", "[token_type]")
{
  REQUIRE(TokenType::START == TokenType::spellingToTokenType("<START>"));
}

TEST_CASE("END token type", "[token_type]")
{
  REQUIRE(TokenType::END == TokenType::spellingToTokenType("<END>"));
}

TEST_CASE("EOL token type", "[token_type]")
{
  REQUIRE(TokenType::EOL == TokenType::spellingToTokenType("\n"));
}

TEST_CASE("Exceptions token type", "[token_type]")
{
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType(""), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("1asdf"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("0asdf"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf!"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as!df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf&"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as&df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf|"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as|df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf->"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as->df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf<->"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as<->df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf("), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as(df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("asdf)"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("as)df"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("<STAART>"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("<EEND>"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("s<START>"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("d<END>"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("<START>a"), NoMatchingTokenTypeException);
 REQUIRE_THROWS_AS(TokenType::spellingToTokenType("<END>f"), NoMatchingTokenTypeException);
}

TEST_CASE("Test typeToString method", "[token_type]")
{
    REQUIRE(TokenType::typeToString(TokenType::VAR) == "VAR");
    REQUIRE(TokenType::typeToString(TokenType::TRUE) == "TRUE");
    REQUIRE(TokenType::typeToString(TokenType::FALSE) == "FALSE");
    REQUIRE(TokenType::typeToString(TokenType::NOT) == "NOT");
    REQUIRE(TokenType::typeToString(TokenType::AND) == "AND");
    REQUIRE(TokenType::typeToString(TokenType::OR) == "OR");
    REQUIRE(TokenType::typeToString(TokenType::IMPL) == "IMPL");
    REQUIRE(TokenType::typeToString(TokenType::BICOND) == "BICOND");
    REQUIRE(TokenType::typeToString(TokenType::LPAREN) == "LPAREN");
    REQUIRE(TokenType::typeToString(TokenType::RPAREN) == "RPAREN");
    REQUIRE(TokenType::typeToString(TokenType::START) == "START");
    REQUIRE(TokenType::typeToString(TokenType::END) == "END");
    REQUIRE(TokenType::typeToString(TokenType::EOL) == "EOL");
}
