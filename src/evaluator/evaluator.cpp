#include <math.h>

#include <TruthTableCreator/evaluator/evaluator.hpp>

#include <TruthTableCreator/syntax/expression/variable_expression.hpp>
#include <TruthTableCreator/syntax/expression/binary_expression.hpp>
#include <TruthTableCreator/syntax/expression/unary_expression.hpp>
#include <TruthTableCreator/syntax/expression/const_expression.hpp>
#include <TruthTableCreator/syntax/expression/and_expression.hpp>
#include <TruthTableCreator/syntax/expression/or_expression.hpp>
#include <TruthTableCreator/syntax/expression/not_expression.hpp>
#include <TruthTableCreator/syntax/expression/implication_expression.hpp>
#include <TruthTableCreator/syntax/expression/biconditional_expression.hpp>

std::map<std::map<std::string, bool>, bool>  Evaluator::evaluateExpression(std::shared_ptr<Expression> expression)
{
    // Generate the variable mappings as result mappings with default results
    std::vector<std::string> variable_names = std::vector<std::string>();
    Evaluator::findVariables(expression, variable_names);
    std::map<std::map<std::string, bool>, bool> result_mappings = Evaluator::generateVariableMappings(variable_names);

    // Iterate over individual variable mappings and assign result
    for(auto& [variable_mapping, result] : result_mappings)
    {
        result = evaluateExpressionFor(expression, variable_mapping);
    }

    return result_mappings;
}

bool Evaluator::evaluateExpressionFor(std::shared_ptr<Expression> expression, 
                                        std::map<std::string, bool> variable_mappings)
{
    if(ConstExpression* const_expression = dynamic_cast<ConstExpression*>(expression.get()))
    {
        return const_expression -> getValue();
    }

    if(NotExpression* not_expression = dynamic_cast<NotExpression*>(expression.get()))
    {
        return !(Evaluator::evaluateExpressionFor(not_expression -> getOperand(), variable_mappings));
    }

    if(AndExpression* and_expression = dynamic_cast<AndExpression*>(expression.get()))
    {
        return Evaluator::evaluateExpressionFor(and_expression -> getLeftHandOperand(), variable_mappings) &&
            Evaluator::evaluateExpressionFor(and_expression -> getRightHandOperand(), variable_mappings);
    }

    if(OrExpression* or_expression = dynamic_cast<OrExpression*>(expression.get()))
    {
        return Evaluator::evaluateExpressionFor(or_expression -> getLeftHandOperand(), variable_mappings) ||
            Evaluator::evaluateExpressionFor(or_expression -> getRightHandOperand(), variable_mappings);
    }

    if(ImplicationExpression* implication_expression = dynamic_cast<ImplicationExpression*>(expression.get()))
    {
        return !(Evaluator::evaluateExpressionFor(implication_expression -> getLeftHandOperand(), variable_mappings)) ||
            Evaluator::evaluateExpressionFor(implication_expression -> getRightHandOperand(), variable_mappings);
    }

    if(BiconditionalExpression* biconditional_expression = dynamic_cast<BiconditionalExpression*>(expression.get()))
    {
        return (Evaluator::evaluateExpressionFor(biconditional_expression -> getLeftHandOperand(), variable_mappings) &&
            Evaluator::evaluateExpressionFor(biconditional_expression -> getRightHandOperand(), variable_mappings)) ||
            (!(Evaluator::evaluateExpressionFor(biconditional_expression -> getLeftHandOperand(), variable_mappings)) &&
            !(Evaluator::evaluateExpressionFor(biconditional_expression -> getRightHandOperand(), variable_mappings)));
    }

    if(VariableExpression* variable_expression = dynamic_cast<VariableExpression*>(expression.get()))
    {
        auto search_result = variable_mappings.find(variable_expression -> getSpelling());
        if(search_result == variable_mappings.end())
        {
            throw RuntimeErrorException("Tried evaluating a variable to which no mapping exists!");
        }
        else
        {
            return search_result -> second; // return variable mapping
        }
    }

    throw RuntimeErrorException("Encountered an unevaluable Expression Type!");
}

void Evaluator::findVariables(std::shared_ptr<Expression> expression, std::vector<std::string> &variable_names)
{
    if(VariableExpression* var_expr = dynamic_cast<VariableExpression*>(expression.get()))
    {
        variable_names.push_back(var_expr -> getSpelling());        
    }

    // Not a variable, keep looking
    else
    {
        // Binary Expression, have to check left and right operand
        if(BinaryExpression* bin_expr = dynamic_cast<BinaryExpression*>(expression.get()))
        {
            findVariables(bin_expr -> getLeftHandOperand(), variable_names);
            findVariables(bin_expr -> getRightHandOperand(), variable_names);
        }

        else if(UnaryExpression* un_expr = dynamic_cast<UnaryExpression*>(expression.get()))
        {
            findVariables(un_expr -> getOperand(), variable_names);
        }
        
    }
}

std::map<std::map<std::string, bool>, bool> Evaluator::generateVariableMappings(std::vector<std::string> variables)
{
    // resulting map
    std::map<std::map<std::string, bool>, bool> variable_mappings = std::map<std::map<std::string, bool>, bool>();
    // Initialize base map
    std::map<std::string, bool> base_map = std::map<std::string, bool>();

    size_t num_variables = variables.size();

    size_t num_possible_combinations = std::pow(2, num_variables);

    for(auto variable : variables)
    {
        base_map.emplace(variable, false);
    }

    // use the iterator i in binary representation to fill 
    for(size_t i = 0; i < num_possible_combinations; ++i)
    {
        size_t tmp = i; // temp size needed
        std::map<std::string, bool> new_map = base_map;
        for(auto& [var, val] : new_map)
        {
            val = tmp & 1;
            tmp = tmp >> 1;
        }

        variable_mappings.emplace(new_map, false);
    }

    return variable_mappings;
}
