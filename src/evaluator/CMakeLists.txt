set(EVALUATOR_HEADER_LIST "${TruthTableCreator_SOURCE_DIR}/include/TruthTableCreator/evaluator/*.hpp")

if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) AND BUILD_TESTING)
  set(CMAKE_BUILD_TYPE Debug)
  set(CMAKE_CXX_FLAGS " ${CMAKE_CXX_FLAGS} -g --coverage -O0 -Wall -fprofile-arcs -ftest-coverage")
endif()

# The evaluator library
add_library(libttc_evaluator evaluator.cpp)

target_include_directories(libttc_evaluator PUBLIC ${TruthTableCreator_SOURCE_DIR}/include)

target_compile_features(libttc_evaluator PUBLIC cxx_std_20)

target_link_libraries(libttc_evaluator libttc_syntax)
