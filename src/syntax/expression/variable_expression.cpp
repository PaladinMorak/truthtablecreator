/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <TruthTableCreator/syntax/expression/variable_expression.hpp>

VariableExpression::VariableExpression() : Expression()
{
    this -> spelling_ = "";
}

VariableExpression::VariableExpression(SourceLocation source_location, std::string spelling) : Expression(source_location)
{
    this -> spelling_ = spelling;
}

std::string VariableExpression::getSpelling() const
{
    return this -> spelling_;
}

VariableExpression::operator std::string() const
{
    std::string variable_as_string =  "VariableExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Spelling: " + std::string(this -> getSpelling());
    return variable_as_string;
}

std::string VariableExpression::toString() const
{
    std::string variable_as_string =  "VariableExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Spelling: " + std::string(this -> getSpelling());
    return variable_as_string;
}

bool VariableExpression::equals(std::shared_ptr<AstNode> variable) const
{
    VariableExpression* variable_to_check = dynamic_cast<VariableExpression*>(variable.get());

    if(variable_to_check)
    {
        std::string this_spelling = this -> getSpelling();
        std::string spelling_to_check = variable_to_check -> getSpelling();

        if(this_spelling == spelling_to_check)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else // Couldn't cast, can't be equal
    {
       return false;
    }
}

std::string VariableExpression::toPrettyString() const
{
    return this -> spelling_;
}
