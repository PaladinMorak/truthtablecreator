/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <TruthTableCreator/syntax/expression/or_expression.hpp>

OrExpression::OrExpression():BinaryExpression(){};

OrExpression::OrExpression(SourceLocation source_location,
                                             std::shared_ptr<Expression> left_hand_operand,
                                             std::shared_ptr<Expression> right_hand_operand) : BinaryExpression(source_location,
                                                                                                left_hand_operand,
                                                                                                right_hand_operand){};

bool OrExpression::equals(std::shared_ptr<AstNode> expr) const
{
    OrExpression* expression_to_check = dynamic_cast<OrExpression*>(expr.get());

    if(expression_to_check)
    {
        if ((this -> right_hand_operand_ -> equals(expression_to_check -> getRightHandOperand())) &&
            (this -> left_hand_operand_ -> equals(expression_to_check -> getLeftHandOperand())))
        {
            return true;

        }
        else // not equal
        {
            return false;
        }
    }
    else // Couldn't cast to Implication Expression
    {
        return false;
    }
}

OrExpression::operator std::string() const
{
   return "OrExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string OrExpression::toString() const
{
   return "OrExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string OrExpression::toPrettyString() const
{
    return "(" + this -> left_hand_operand_ -> toPrettyString()+ ") | (" + this-> right_hand_operand_ -> toPrettyString() + ")";
}
