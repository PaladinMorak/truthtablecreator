/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/expression/const_expression.hpp"

ConstExpression::ConstExpression() : Expression()
{
  this -> type_ = NodeType::ConstExpression;
  this -> value_ = false;
}

ConstExpression::ConstExpression(SourceLocation source_location, bool value) : Expression(source_location)
{
  this -> type_ = NodeType::ConstExpression;
  this -> value_ = value;
}

bool ConstExpression::getValue() const
{
  return this -> value_;
}

#include <iostream>
bool ConstExpression::equals(std::shared_ptr<AstNode> expression) const
{
  const ConstExpression* expression_to_check = dynamic_cast<const ConstExpression*>(expression.get());

  if(expression_to_check)
  {
    bool own_val = this -> value_;
    bool val_to_check = expression_to_check -> getValue();
    return (own_val == val_to_check);
  }

  return false; // Couldn't cast to ConstExpression, can't be equal!
}

ConstExpression::operator std::string() const
{
    return "ConstExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Value: " + std::string(this -> getValue() ? "true" : "false");
}

std::string ConstExpression::toString() const
{
    return "ConstExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Value: " + std::string(this -> getValue() ? "true" : "false");
}

std::string ConstExpression::toPrettyString() const
{
    return std::to_string(this -> value_);
}
