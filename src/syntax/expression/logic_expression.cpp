/**
 * @author Georg Bettenhausen
 * @date July 2023
 */

#include <TruthTableCreator/syntax/expression/logic_expression.hpp>

LogicExpression::LogicExpression(SourceLocation source_location, std::list<std::string> variables) : AstNode(source_location)
{
    this -> variables_ = variables;
}

std::list<std::string> LogicExpression::getVariables() const
{
   return this -> variables_;
}
