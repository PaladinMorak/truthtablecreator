/**
 * @author Georg Bettenhausen
 * @date November 2022
 */
#include <iostream>

#include "TruthTableCreator/syntax/expression/binary_expression.hpp"
#include "TruthTableCreator/syntax/expression/const_expression.hpp"
#include "TruthTableCreator/syntax/expression/expression.hpp"
#include "TruthTableCreator/syntax/expression/unary_expression.hpp"
#include "TruthTableCreator/syntax/node_type.hpp"

Expression::Expression() : AstNode() { this->type_ = NodeType::Expression; }

Expression::Expression(SourceLocation source_location)
    : AstNode(source_location) {
  this->type_ = NodeType::Expression;
}

bool Expression::equals(std::shared_ptr<AstNode> expr) const {
  // The type of this node and the given expression have to match
  NodeType::type own_type = this->getType();
  NodeType::type expression_type = expr->getType();
  if (!(own_type == expression_type)) {
    return false;
  }

  // Use the type of the node in order to use the correct equals statement
  switch (own_type) {
  case NodeType::ConstExpression: {
    const ConstExpression *this_expr =
        dynamic_cast<const ConstExpression *>(this);

    // Could not this to ConstExpression even though the Expression type
    // indicated it should be a ConstExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a ConstExpression!");
    }

    return this_expr->equals(expr);
  }
  case NodeType::UnaryExpression: {
    const UnaryExpression *this_expr =
        dynamic_cast<const UnaryExpression *>(this);

    // Could not this to UnaryExpression even though the Expression type
    // indicated it should be a UnaryExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a UnaryExpression!");
    }

    return this_expr->equals(expr);
  }
  case NodeType::BinaryExpression: {
    const BinaryExpression *this_expr =
        dynamic_cast<const BinaryExpression *>(this);

    // Could not this to UnaryExpression even though the Expression type
    // indicated it should be a BinaryExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a BinaryExpression!");
    }

    return this_expr->equals(expr);
  }
  default: {
    return (this->getSourceLocation().equals(expr->getSourceLocation()));
  }
  }
}

Expression::operator std::string() const {
  return "Expression, Source Location: " +
         std::string(this->getSourceLocation());
}

std::string Expression::toString() const {
  // The type of this node and the given expression have to match
  NodeType::type own_type = this->getType();

  // Use the type of the node in order to use the correct equals statement
  switch (own_type) {
  case NodeType::ConstExpression: {
    const ConstExpression *this_expr =
        dynamic_cast<const ConstExpression *>(this);

    // Could not this to ConstExpression even though the Expression type
    // indicated it should be a ConstExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a ConstExpression!");
    }

    return this_expr -> toString();
  }
  case NodeType::UnaryExpression: {
    const UnaryExpression *this_expr =
        dynamic_cast<const UnaryExpression *>(this);

    // Could not this to UnaryExpression even though the Expression type
    // indicated it should be a UnaryExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a UnaryExpression!");
    }

    return this_expr -> toString();
  }
  case NodeType::BinaryExpression: {
    const BinaryExpression *this_expr =
        dynamic_cast<const BinaryExpression *>(this);

    // Could not this to UnaryExpression even though the Expression type
    // indicated it should be a BinaryExpression May be due to the fact that
    // object wasn't saved as the correct class or not as a pointer
    if (!this_expr) {
      throw UnableToCastToRightExpressionTypeException(
          "Could not cast the given expression to a BinaryExpression!");
    }

    return this_expr -> toString();
  }
  default: {
      return "Expression, Source Location: " + std::string(this->getSourceLocation());
  }
  }
}

std::ostream& operator <<( std::ostream& os, Expression const& node )
{
    return (os << node.toString());
}

std::string Expression::toPrettyString() const
{
    return this -> toString();
}
