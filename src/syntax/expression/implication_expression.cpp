/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <TruthTableCreator/syntax/expression/implication_expression.hpp>

ImplicationExpression::ImplicationExpression():BinaryExpression(){};

ImplicationExpression::ImplicationExpression(SourceLocation source_location,
                                             std::shared_ptr<Expression> left_hand_operand,
                                             std::shared_ptr<Expression> right_hand_operand) : BinaryExpression(source_location,
                                                                                                left_hand_operand,
                                                                                                right_hand_operand){};

bool ImplicationExpression::equals(std::shared_ptr<AstNode> expr) const
{
    ImplicationExpression* expression_to_check = dynamic_cast<ImplicationExpression*>(expr.get());

    if(expression_to_check)
    {
        if ((this -> right_hand_operand_ -> equals(expression_to_check -> getRightHandOperand())) &&
            (this -> left_hand_operand_ -> equals(expression_to_check -> getLeftHandOperand())))
        {
            return true;

        }
        else // not equal
        {
            return false;
        }
    }
    else // Couldn't cast to Implication Expression
    {
        return false;
    }
}

ImplicationExpression::operator std::string() const
{
   return "ImplicationExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string ImplicationExpression::toString() const
{
   return "ImplicationExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string ImplicationExpression::toPrettyString() const
{
    return "(" + this -> left_hand_operand_ -> toPrettyString()+ ") -> (" + this-> right_hand_operand_ -> toPrettyString() + ")";
}
