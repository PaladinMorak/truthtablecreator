/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/expression/not_expression.hpp"

NotExpression::NotExpression() : UnaryExpression(){}

NotExpression::NotExpression(SourceLocation source_location, std::shared_ptr<Expression> operand) :
  UnaryExpression(source_location, operand){}

NotExpression::operator std::string() const
{
    return "NotExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Operand: " + std::string(*this -> getOperand());
}

bool NotExpression::equals(std::shared_ptr<AstNode> expr) const
{
  const NotExpression* expression_to_check = dynamic_cast<const NotExpression*>(expr.get());

  if(expression_to_check)
  {
    // Unary Expressions are equal if their operands are equal
    std::shared_ptr<Expression> operand_to_check = expression_to_check -> getOperand();
    std::shared_ptr<Expression> this_operand = this -> getOperand();
    return (this_operand -> equals(operand_to_check));
  }

  return false; // Couldn't cast to UnaryExpression, can't be equal!
}

std::string NotExpression::toString() const
{
    return "NotExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Operand: " + std::string(*this -> getOperand());
}

std::string NotExpression::toPrettyString() const
{
    return "!(" + this -> operand_ -> toPrettyString()  + ")";
}
