/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/expression/and_expression.hpp"

AndExpression::AndExpression() : BinaryExpression(){};

AndExpression::AndExpression(SourceLocation source_location,
                             std::shared_ptr<Expression> leftHandOperand,
                             std::shared_ptr<Expression> rightHandOperand) : BinaryExpression(source_location,
                                                                             leftHandOperand,
                                                                             rightHandOperand) {};

bool AndExpression::equals(std::shared_ptr<AstNode> expr) const
{
    AndExpression* expression_to_check = dynamic_cast<AndExpression*>(expr.get());

    if(expression_to_check)
    {
        if((this -> left_hand_operand_ -> equals(expression_to_check -> getLeftHandOperand())) &&
           (this -> right_hand_operand_ -> equals(expression_to_check -> getRightHandOperand())))
        {
            return true;
        } else
        {
           return false;
        }
    } else // Couldn't cast
    {
        return false;
    }
}

AndExpression::operator std::string() const
{
   return "AndExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string AndExpression::toString() const
{
   return "AndExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string AndExpression::toPrettyString() const
{
    return "(" + this -> left_hand_operand_ -> toPrettyString()+ ") & (" + this-> right_hand_operand_ -> toPrettyString() + ")";
}
