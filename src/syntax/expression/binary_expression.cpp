/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/expression/binary_expression.hpp"

BinaryExpression::BinaryExpression() : Expression()
{
  this -> type_ = NodeType::BinaryExpression;
  this -> left_hand_operand_ = std::make_shared<Expression>(Expression());
  this -> right_hand_operand_ = std::make_shared<Expression>(Expression());
}

BinaryExpression::BinaryExpression(SourceLocation source_location,
                                   std::shared_ptr<Expression> left_hand_operand,
                                   std::shared_ptr<Expression> right_hand_operand) : Expression(source_location)
{
  this -> type_ = NodeType::BinaryExpression;
  this -> left_hand_operand_ = left_hand_operand;
  this -> right_hand_operand_ = right_hand_operand;
}

std::shared_ptr<Expression> BinaryExpression::getLeftHandOperand() const
{
  return this -> left_hand_operand_;
}

std::shared_ptr<Expression> BinaryExpression::getRightHandOperand() const
{
  return this -> right_hand_operand_;
}

bool BinaryExpression::equals(std::shared_ptr<AstNode> expr) const
{
  BinaryExpression* expression_to_check = dynamic_cast<BinaryExpression*>(expr.get());

  if(expression_to_check)
  {
    // Binary Expressions are equal if their operands are equal
    std::shared_ptr<Expression> right_hand_operand_to_check = expression_to_check -> getRightHandOperand();
    bool right_operand_equal = (this -> right_hand_operand_) -> equals(right_hand_operand_to_check);
    std::shared_ptr<Expression> left_hand_operand_to_check = expression_to_check -> getLeftHandOperand();
    bool left_operand_equal = (this -> left_hand_operand_) -> equals(left_hand_operand_to_check);

    return right_operand_equal && left_operand_equal;
  }

  return false; // couldn't cast to binary expression, can't be equal
}

BinaryExpression::operator std::string() const
{
   return "BinaryExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string BinaryExpression::toString() const
{
   return "BinaryExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}
