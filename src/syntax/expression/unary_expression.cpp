/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/expression/unary_expression.hpp"

UnaryExpression::UnaryExpression() : Expression()
{
  this -> type_ = NodeType::UnaryExpression;
  this -> operand_ = std::make_shared<Expression>(Expression());
}

UnaryExpression::UnaryExpression(SourceLocation source_location, std::shared_ptr<Expression> operand) : Expression(source_location)
{
  this -> type_ = NodeType::UnaryExpression;
  this -> operand_ = operand;
}

std::shared_ptr<Expression> UnaryExpression::getOperand() const
{
  return this -> operand_;
}

bool UnaryExpression::equals(std::shared_ptr<AstNode> expr) const
{
  const UnaryExpression* expression_to_check = dynamic_cast<const UnaryExpression*>(expr.get());

  if(expression_to_check)
  {
    // Unary Expressions are equal if their operands are equal
      std::shared_ptr<Expression> operand_to_check = expression_to_check -> getOperand();
    return this -> operand_ -> equals(operand_to_check);
  }

  return false; // Couldn't cast to UnaryExpression, can't be equal!
}

UnaryExpression::operator std::string() const
{
    return "UnaryExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Operand: " + std::string(*(this -> operand_));
};

std::string UnaryExpression::toString() const
{
    return "UnaryExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Operand: " + std::string(*(this -> operand_));
};
