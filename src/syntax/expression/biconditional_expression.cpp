/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <TruthTableCreator/syntax/expression/biconditional_expression.hpp>

BiconditionalExpression::BiconditionalExpression():BinaryExpression(){};

BiconditionalExpression::BiconditionalExpression(SourceLocation source_location,
                                             std::shared_ptr<Expression> left_hand_operand,
                                             std::shared_ptr<Expression> right_hand_operand) : BinaryExpression(source_location,
                                                                                                left_hand_operand,
                                                                                                right_hand_operand){};

bool BiconditionalExpression::equals(std::shared_ptr<AstNode> expr) const
{
    BiconditionalExpression* expression_to_check = dynamic_cast<BiconditionalExpression*>(expr.get());

    if(expression_to_check)
    {
        if ((this -> right_hand_operand_ -> equals(expression_to_check -> getRightHandOperand())) &&
            (this -> left_hand_operand_ -> equals(expression_to_check -> getLeftHandOperand())))
        {
            return true;

        }
        else // not equal
        {
            return false;
        }
    }
    else // Couldn't cast to Implication Expression
    {
        return false;
    }
}

BiconditionalExpression::operator std::string() const
{
   return "BiconditionalExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string BiconditionalExpression::toString() const
{
   return "BiconditionalExpression, Source Location: " + std::string(this -> getSourceLocation()) + "; Left-Hand Operand: " + std::string(*(this -> left_hand_operand_)) + "; Right-Hand Operand: " + std::string(*(this -> right_hand_operand_));
}

std::string BiconditionalExpression::toPrettyString() const
{
    return "(" + this -> left_hand_operand_ -> toPrettyString()+ ") <-> (" + this-> right_hand_operand_ -> toPrettyString() + ")";
}
