#include <TruthTableCreator/syntax/node_type.hpp>

std::string NodeType::typeToString(NodeType::type typeToConvert)
{
    switch(typeToConvert)
    {
        case NodeType::Node:
            return std::string("Node");
        case NodeType::Expression:
            return std::string("Expression");
        case NodeType::UnaryExpression:
            return std::string("UnaryExpression");
        case NodeType::BinaryExpression:
            return std::string("BinaryExpression");
        case NodeType::ConstExpression:
            return std::string("ConstExpression");
    }
}
