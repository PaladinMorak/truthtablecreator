/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include "TruthTableCreator/syntax/source_location.hpp"

SourceLocation::SourceLocation()
{
  this -> line_ = 0;
  this -> column_ = 0;
}

SourceLocation::SourceLocation(unsigned short line, unsigned short column)
{
  this -> line_ = line;
  this -> column_ = column;
}

unsigned short SourceLocation::getLine() const
{
  return this -> line_;
}

unsigned short SourceLocation::getColumn() const
{
  return this -> column_;
}

SourceLocation::operator std::string() const
{
    return "line: " + std::to_string(this -> line_) + ", column: " + std::to_string(this -> column_);
};

std::ostream& operator <<( std::ostream& os, SourceLocation const& location ) {
    os << std::string(location);
    return os;
}

bool SourceLocation::equals(SourceLocation location) const
{
  return ((this -> line_ == location.getLine()) && (this -> column_ == location.getColumn()));
}

bool operator ==  (SourceLocation const& lhs, SourceLocation const& rhs)
{
  return lhs.equals(rhs);
}
