#include "TruthTableCreator/syntax/ast_node.hpp"

AstNode::AstNode()
{
  this -> type_ = NodeType::Node;
  this -> source_location_ = SourceLocation();
}

AstNode::AstNode (SourceLocation location)
{
  this -> type_ = NodeType::Node;
  this -> source_location_ = location;
}

SourceLocation AstNode::getSourceLocation() const
{
  return this -> source_location_;
}

bool AstNode::equals(std::shared_ptr<AstNode> node) const
{
  return this -> source_location_ == node -> getSourceLocation();
}

std::ostream& operator <<( std::ostream& os, AstNode const& node )
{
  os << std::string(node);
  return os;
}

AstNode::operator std::string() const
{
    return "AstNode, Source Location: " + std::string(this -> source_location_);
};

std::string AstNode::toString() const
{
    return "AstNode, Source Location: " + std::string(this -> source_location_);
}

NodeType::type AstNode::getType() const
{
  return this -> type_;
}
