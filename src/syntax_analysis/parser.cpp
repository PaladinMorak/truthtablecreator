/**
 * @author Georg Bettenhausen
 * @date June 2023
 */

#include <TruthTableCreator/syntax_analysis/parser.hpp>

Parser::Parser(std::deque<Token> tokens)
{
    this -> tokens_ = tokens;
    this -> current_token_ = tokens.front();
    this -> tokens_.pop_front(); // have to remove first element that is now in current_token_
}

std::list<std::shared_ptr<Expression>> Parser::parse()
{
    this -> logic_expressions_ = std::list<std::shared_ptr<Expression>>();

    acceptTokenType(TokenType::START);
    acceptTokenType(TokenType::EOL);
    while(!isEndToken(this -> current_token_))
    {
        std::shared_ptr<Expression> expression = parseExpression();
        if(isEOLToken(this -> current_token_))
        {
            if (!this -> current_expression_stack_.empty())
            {
                throw SyntaxErrorException("Found more than one expression in line: " + std::to_string(this -> current_token_.getTokenLine()));
            }
            else
            {
                this -> logic_expressions_.push_back(expression);
                acceptToken(); // accept EOL
            }
        }
        else
        {
            this -> current_expression_stack_.push(expression);
        }
    }
    acceptTokenType(TokenType::END); // accep end token

    return this -> logic_expressions_;
}

void Parser::acceptToken()
{
    if(this -> tokens_.empty())
    {
        throw RuntimeErrorException("Encountered an empty token list during parsing!");
    }

    this -> current_token_ = this -> tokens_.front();
    this -> tokens_.pop_front();
}

void Parser::checkTokenType(TokenType::type expected_type)
{
    TokenType::type current_token_type = this -> current_token_.getTokenType();

    if(current_token_type != expected_type)
    {
        throw SyntaxErrorException("TokenType mismatch, the given token: " + std::string(this -> current_token_) +
                                   " does not match the expected type: " + TokenType::typeToString(expected_type));
    }
}

std::string Parser::acceptTokenType(TokenType::type expected_type)
{
    // Check if current token's type is equal to expected, otherwise throw SyntaxErrorException
    this -> checkTokenType(expected_type);

    // Accept token returning the spelling
    std::string current_token_spelling = this -> current_token_.getTokenSpelling();

    acceptToken();

    return current_token_spelling;
}

std::shared_ptr<Expression> Parser::parseExpression()
{
    switch (this -> current_token_.getTokenType()) {
        case TokenType::VAR:
        {
            return parseVariable();
            break;
        }
        case TokenType::TRUE:
        {
            return parseConstExpression();
            break;
        }
        case TokenType::FALSE:
        {
            return parseConstExpression();
            break;
        }
        case TokenType::NOT:
        {
            return parseNotExpression();
            break;
        }
        case TokenType::AND:
        {
            if(!this -> current_expression_stack_.empty())
            {
                // get latest item pushed onto stack of logic expressions, must be left hand operand
                std::shared_ptr<Expression> left_hand_operand = this -> current_expression_stack_.top();
                this -> current_expression_stack_.pop();
                return parseBinaryExpression(left_hand_operand);
            }
            else // no left hand operand available, throw Syntax Error
            {
                throw SyntaxErrorException("No left-hand operand provided for AND expression at: " + std::string(this -> current_token_.getSourceLocation()));
            }
            break;
        }
        case TokenType::OR:
        {
            if(!this -> current_expression_stack_.empty())
            {
                // get latest item pushed onto stack of logic expressions, must be left hand operand
                std::shared_ptr<Expression> left_hand_operand = this -> current_expression_stack_.top();
                this -> current_expression_stack_.pop();
                return parseBinaryExpression(left_hand_operand);
            }
            else // no left hand operand available, throw Syntax Error
            {
                throw SyntaxErrorException("No left-hand operand provided for OR expression at: " + std::string(this -> current_token_.getSourceLocation()));
            }
            break;
        }
        case TokenType::IMPL:
        {
            if(!this -> current_expression_stack_.empty())
            {
                // get latest item pushed onto stack of logic expressions, must be left hand operand
                std::shared_ptr<Expression> left_hand_operand = this -> current_expression_stack_.top();
                this -> current_expression_stack_.pop();
                return parseBinaryExpression(left_hand_operand);
            }
            else // no left hand operand available, throw Syntax Error
            {
                throw SyntaxErrorException("No left-hand operand provided for IMPLICATION expression at: " + std::string(this -> current_token_.getSourceLocation()));
            }
            break;
        }
        case TokenType::BICOND:
        {
            if(!this -> current_expression_stack_.empty())
            {
                // get latest item pushed onto stack of logic expressions, must be left hand operand
                std::shared_ptr<Expression> left_hand_operand = this -> current_expression_stack_.top();
                this -> current_expression_stack_.pop();
                return parseBinaryExpression(left_hand_operand);
            }
            else // no left hand operand available, throw Syntax Error
            {
                throw SyntaxErrorException("No left-hand operand provided for BICONDITIONAL expression at: " + std::string(this -> current_token_.getSourceLocation()));
            }
            break;
        }
        case TokenType::LPAREN:
        {
            acceptToken();
            while(!(this -> current_token_.getTokenType() == TokenType::RPAREN))
            {
                this -> current_expression_stack_.push(parseExpression());
            }
            acceptTokenType(TokenType::RPAREN); // Need to get closing paranthesis
            

            if(this -> current_expression_stack_.empty())
            {
                throw SyntaxErrorException("Found an empty paranthesis at: " + std::string(this -> current_token_.getSourceLocation()));
            }

            std::shared_ptr<Expression> parsed_expression = this -> current_expression_stack_.top();
            this -> current_expression_stack_.pop();
            return parsed_expression;
            break;
        }
        case TokenType::RPAREN:
        {
            // Shouldn't find a right paranthesis floating around, all closing paranthesis are dealt with separately
            throw SyntaxErrorException("Unexpected closing paranthese at: " + std::string(this -> current_token_.getSourceLocation()));
            break;
        }
        case TokenType::START:
        {
            // Shouldn't find a START token floating around
            throw SyntaxErrorException("Unexpected START command at: " + std::string(this -> current_token_.getSourceLocation()));
            break;
        }
        case TokenType::END:
        {
            if(!(this -> current_expression_stack_.empty()))
            {
                std::shared_ptr<Expression> expression = this -> current_expression_stack_.top();
                this -> current_expression_stack_.pop();
                return expression;
            }

            // Shouldn't find an END token floating around
            throw SyntaxErrorException("Unexpected END command at: " + std::string(this -> current_token_.getSourceLocation()));
            break;
        }
        case TokenType::EOL:
        {
            throw SyntaxErrorException("Unexpected EOL command at: " + std::string(this -> current_token_.getSourceLocation()));
            break;
        }
        default:
        {
            throw SyntaxErrorException("Unexpected syntax error at: " + std::string(this -> current_token_.getSourceLocation()));
            break;
        }
    }
}

std::shared_ptr<BinaryExpression> Parser::parseBinaryExpression(std::shared_ptr<Expression> left_hand_operand)
{
    SourceLocation location = this -> current_token_.getSourceLocation();

    TokenType::type current_token_type = this -> current_token_.getTokenType();
    switch (current_token_type) {
        case TokenType::AND:
        {
            return parseAndExpression(left_hand_operand);
        }
        case TokenType::OR:
        {
            return parseOrExpression(left_hand_operand);
        }
        case TokenType::IMPL:
        {
            return parseImplicationExpression(left_hand_operand);
        }
        case TokenType::BICOND:
        {
            return parseBiconditionalExpression(left_hand_operand);
        }
        default:
        {
            throw SyntaxErrorException("Unexpected syntax error at: " + std::string(location));
        }
    }
}

std::shared_ptr<AndExpression> Parser::parseAndExpression(std::shared_ptr<Expression> left_hand_operand)
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    acceptToken(); // accept and token
    std::shared_ptr<Expression> right_hand_operand = parseExpression();
    return std::make_shared<AndExpression>(AndExpression(location, left_hand_operand, right_hand_operand));
}

std::shared_ptr<BiconditionalExpression> Parser::parseBiconditionalExpression(std::shared_ptr<Expression> left_hand_operand)
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    acceptToken(); // accept BICOND token
    std::shared_ptr<Expression> right_hand_operand = parseExpression();
    return std::make_shared<BiconditionalExpression>(BiconditionalExpression(location, left_hand_operand, right_hand_operand));
}

std::shared_ptr<ImplicationExpression> Parser::parseImplicationExpression(std::shared_ptr<Expression> left_hand_operand)
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    acceptToken(); // accept IMP token
    std::shared_ptr<Expression> right_hand_operand = parseExpression();
    return std::make_shared<ImplicationExpression>(ImplicationExpression(location, left_hand_operand, right_hand_operand));
}

std::shared_ptr<OrExpression> Parser::parseOrExpression(std::shared_ptr<Expression> left_hand_operand)
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    acceptToken(); // accept OR token
    std::shared_ptr<Expression> right_hand_operand = parseExpression();
    return std::make_shared<OrExpression>(OrExpression(location, left_hand_operand, right_hand_operand));
}

bool Parser::isEndToken(Token token) const
{
    return token.getTokenType() == TokenType::END;
}

bool Parser::isEOLToken(Token token) const
{
    return token.getTokenType() == TokenType::EOL;
}

std::shared_ptr<VariableExpression> Parser::parseVariable()
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    std::string spelling = acceptTokenType(TokenType::VAR);

    return std::make_shared<VariableExpression>(VariableExpression(location, spelling));
}

std::shared_ptr<NotExpression> Parser::parseNotExpression()
{
    SourceLocation location = this -> current_token_.getSourceLocation();

    acceptToken(); // accept not token
    std::shared_ptr<Expression> operand = parseExpression();

    return std::make_shared<NotExpression>(NotExpression(location, operand));
}

std::shared_ptr<ConstExpression> Parser::parseConstExpression()
{
    SourceLocation location = this -> current_token_.getSourceLocation();
    TokenType::type current_token_type = this -> current_token_.getTokenType();
    acceptToken(); // accept constant

    bool value = false; // Set value of Const expression to false
    if(current_token_type == TokenType::TRUE)
    {
        value = true; // If TokenType == TRUE then set value to true, otherwise already false
    }

    return std::make_shared<ConstExpression>(ConstExpression(location, value));
}
