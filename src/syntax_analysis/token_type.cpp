/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <regex>

#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>
#include <TruthTableCreator/syntax_analysis/token_type.hpp>

TokenType::type TokenType::spellingToTokenType(std::string spelling)
{
    if (regex_match(spelling, std::regex("[a-zA-z][a-zA-Z0-9]+")))
    {
        return TokenType::VAR;
    } else if(regex_match(spelling, std::regex("1")))
    {
        return TokenType::TRUE;
    } else if(regex_match(spelling, std::regex("0")))
    {
        return TokenType::FALSE;
    } else if(regex_match(spelling, std::regex("!")))
    {
        return TokenType::NOT;
    } else if(regex_match(spelling, std::regex("&")))
    {
        return TokenType::AND;
    } else if(regex_match(spelling, std::regex("\\|")))
    {
        return TokenType::OR;
    } else if(regex_match(spelling, std::regex("->")))
    {
        return TokenType::IMPL;
    } else if(regex_match(spelling, std::regex("<->")))
    {
        return TokenType::BICOND;
    } else if(regex_match(spelling, std::regex("\\(")))
    {
        return TokenType::LPAREN;
    } else if(regex_match(spelling, std::regex("\\)")))
    {
        return TokenType::RPAREN;
    } else if(regex_match(spelling, std::regex("<START>")))
    {
        return TokenType::START;
    } else if(regex_match(spelling, std::regex("<END>")))
    {
        return TokenType::END;
    } else if(spelling == "\n")
    {
        return TokenType::EOL;
    }

    throw NoMatchingTokenTypeException("No Matching Token exists for the spelling: \"" + spelling + "\"");
};

std::string TokenType::typeToString(TokenType::type type_to_convert)
{
   switch(type_to_convert)
   {
        case(TokenType::VAR):
            return "VAR";
            break;
        case(TokenType::TRUE):
            return "TRUE";
            break;
        case(TokenType::FALSE):
            return "FALSE";
            break;
        case(TokenType::NOT):
            return "NOT";
            break;
        case(TokenType::AND):
            return "AND";
            break;
        case(TokenType::OR):
            return "OR";
            break;
        case(TokenType::IMPL):
            return "IMPL";
            break;
        case(TokenType::BICOND):
            return "BICOND";
            break;
        case(TokenType::LPAREN):
            return "LPAREN";
            break;
        case(TokenType::RPAREN):
            return "RPAREN";
            break;
        case(TokenType::START):
            return "START";
            break;
        case(TokenType::END):
            return "END";
            break;
        case(TokenType::EOL):
            return "EOL";
            break;
        default:
            return "ERROR"; // return error
            break;
   }
}
