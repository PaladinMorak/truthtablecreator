/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <TruthTableCreator/syntax_analysis/token.hpp>

Token::Token()
{
    this -> type_ = TokenType::START;
    this -> spelling_ = "";
    this -> line_ = 0;
    this -> column_ = 0;
}

Token::Token(TokenType::type type, std::string spelling, unsigned short line, unsigned short column)
{
    this -> type_ = type;
    this -> spelling_ = spelling;
    this -> line_ = line;
    this -> column_ = column;
}

bool Token::equals(const Token token) const
{
   if((this -> type_ == token.getTokenType()) && (this -> spelling_ == token.getTokenSpelling()) && (this -> line_ == token.getTokenLine()) && (this -> column_ == token.getTokenColumn()))
   {
       return true;
   }
   else
   {
       return false;
   }
}

TokenType::type Token::getTokenType() const
{
    return this -> type_;
}

std::string Token::getTokenSpelling() const
{
    return this -> spelling_;
}

int Token::getTokenLine() const
{
    return this -> line_;
}

int Token::getTokenColumn() const
{
    return this -> column_;
}

bool operator ==(const Token& lhs, const Token& rhs)
{
    return lhs.equals( rhs );
}

std::ostream& operator <<( std::ostream& os, Token const& token ) {
    os << std::string(token);
    return os;
}

Token::operator std::string() const
{
    if (this -> spelling_ == "\n")
    {
        return TokenType::typeToString(this -> type_) + ": \\n" + " l: " + std::to_string(this -> line_) + " c: " + std::to_string(this -> column_);
    }
    else
    {
        return TokenType::typeToString(this -> type_) + ": " + this -> spelling_ + " l: " + std::to_string(this -> line_) + " c: " + std::to_string(this -> column_);
    }
};

SourceLocation Token::getSourceLocation() const
{
    SourceLocation location = SourceLocation(this -> line_, this -> column_);
    return location;
}
