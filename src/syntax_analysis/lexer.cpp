/**
 * @author Georg Bettenhausen
 * @date November 2022
 */

#include <TruthTableCreator/syntax_analysis/lexer.hpp>
#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>
#include <TruthTableCreator/exceptions/unexpected_eof_exception.hpp>
#include <TruthTableCreator/exceptions/file_error_exception.hpp>

Lexer::Lexer(std::ifstream *input_file)
{
    if(!input_file -> good())
    {
        throw FileErrorException("Encountered a problem while trying to open the file!");
    }
    this -> input_file_ = input_file;
}


std::deque<Token> Lexer::scan()
{
    std::deque<Token> tokens = std::deque<Token>();
    this -> input_file_ -> get(this -> current_char_); // read the first character of the file into current char

    while(!(this -> input_file_ -> eof()))
    {
        // skip white spaces
        while (this -> current_char_ == ' ' || this -> current_char_ == '\t' || this -> current_char_ == '\r')
        {
            this -> skipChar();
        }

        // reset currentSpelling. last line and last column
        this -> current_spelling_ = "";
        this -> last_line_ = this -> current_line_;
        this -> last_column_ = this -> current_column_;

        TokenType::type current_type = this -> scanToken();
        Token newToken = Token(current_type, this -> current_spelling_, this -> last_line_, this -> last_column_);
        tokens.push_back(newToken);
    }

    return tokens;
}

void Lexer::skipChar() {
    if (this -> current_char_ == '\n')
    {
        this -> current_line_ += 1;
        this -> current_column_ = 0;
    } else
    {
        this -> current_column_ += 1;
    }

    // Check if reached eof while trying to skip
    if (this -> input_file_ -> eof())
    {
        std::string err_message = "Unexpectedly reached the end of the file while scanning the token started at line: "
            + std::to_string(this -> last_line_) + " and column: " + std::to_string(this -> last_column_);
        throw UnexpectedEOFException(err_message);
    }

    // Get next char from input file and put into current char
    this -> input_file_ -> get(this -> current_char_);
}

void Lexer::takeChar()
{
    this -> current_spelling_ += current_char_;
    this -> skipChar(); // call skip char here since all counter and exception management is already handled by it
}

TokenType::type Lexer::scanToken()
{
    // Chekc if we're dealing with a variable and scan it if so
    if (isLetter(this -> current_char_))
    {
        return this -> scanVariable();
    }


    char cc = this -> current_char_;
    this -> takeChar();

    // Deal with all other tokens
    try
    {
        // check if it's a single character token
        std::string character_to_analyze = std::string(1, cc); // have to conver this way because otherwise '\n' will not match
                                                      // i.e. with std::to_string(cc)
        return TokenType::spellingToTokenType(character_to_analyze);
    } catch(NoMatchingTokenTypeException& exception)
    {

        switch(cc)
        {
            case('-'): // can be a ->
                if(this -> current_char_ == '>')
                {
                    this -> takeChar();
                   return TokenType::IMPL;
                }
                break;
            case('<'): // can be a <->
                if(this -> current_char_ == '-')
                {
                    return this -> scanBiConditional();
                } else if (this -> current_char_ == 'S')
                {
                    return this -> scanStart();
                } else if (this -> current_char_ == 'E')
                {
                    return this -> scanEnd();
                }
                break;
        }

        // no matching token was found throw exception
        throw NoMatchingTokenTypeException("No Matching Token exists for the spelling \"" + this -> current_spelling_
                                           + "\" started at line: "
                                           + std::to_string(this -> last_line_) + " and column: "
                                           + std::to_string(this -> last_column_));
    }
}

TokenType::type Lexer::scanVariable()
{
    this -> takeChar();
    while(isLetter(this -> current_char_) || isDigit(this -> current_char_))
    {
        this -> takeChar();
    }

    return TokenType::VAR;
}

TokenType::type Lexer::scanBiConditional()
{
    char cc = this -> current_char_;
    if(cc == '-')
    {
    this -> takeChar();
    cc = this -> current_char_;
    if(cc == '>')
        {
        this -> takeChar();
        return TokenType::spellingToTokenType(this -> current_spelling_);
        }
    }

    // One of the if statements wasn't fulfilled -> wrong spelling
    throw NoMatchingTokenTypeException("No Matching Token exists for the spelling \"" + this -> current_spelling_
                                        + "\" started at line: "
                                        + std::to_string(this -> last_line_) + " and column: "
                                        + std::to_string(this -> last_column_));
}

TokenType::type Lexer::scanStart()
{
    char cc = this -> current_char_;
    if(cc == 'S')
    {
        this -> takeChar();
        cc = this -> current_char_;
        if(cc == 'T')
        {
            this -> takeChar();
            cc = this -> current_char_;
            if(cc == 'A')
            {
                this -> takeChar();
                cc = this -> current_char_;
                if(cc == 'R')
                {
                    this -> takeChar();
                    cc = this -> current_char_;
                    if(cc == 'T')
                    {
                    this -> takeChar();
                    cc = this -> current_char_;
                    if(cc == '>')
                        {
                        this -> takeChar();
                        return TokenType::spellingToTokenType(this -> current_spelling_);
                        }
                    }
                }
            }
        }
    }

    // One of the if statements wasn't fulfilled -> wrong spelling
    throw NoMatchingTokenTypeException("No Matching Token exists for the spelling \"" + this -> current_spelling_
                                        + "\" started at line: "
                                        + std::to_string(this -> last_line_) + " and column: "
                                        + std::to_string(this -> last_column_));
}

TokenType::type Lexer::scanEnd()
{
    char cc = this -> current_char_;
    if(cc == 'E')
    {
        this -> takeChar();
        cc = this -> current_char_;
        if(cc == 'N')
        {
            this -> takeChar();
            cc = this -> current_char_;
            if(cc == 'D')
            {
            this -> takeChar();
            cc = this -> current_char_;
            if(cc == '>')
                {
                this -> takeChar();
                return TokenType::spellingToTokenType(this -> current_spelling_);
                }
            }
        }
    }

    // One of the if statements wasn't fulfilled -> wrong spelling
    throw NoMatchingTokenTypeException("No Matching Token exists for the spelling \"" + this -> current_spelling_
                                        + "\" started at line: "
                                        + std::to_string(this -> last_line_) + " and column: "
                                        + std::to_string(this -> last_column_));
}

bool Lexer::isLetter(char character)
{
   if((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z'))
   {
       return true;
   }
   else
   {
       return false;
   }
}

bool Lexer::isDigit(char character)
{
   if(character >= '0' && character <= '9')
   {
       return true;
   }
   else
   {
       return false;
   }
}
