#!/bin/bash

mkdir $PWD/coverage/ 2> /dev/null
cd $PWD/coverage
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root "$PWD/../" $PWD/../build/src
gcovr --html-details --exclude-unreachable-branches --print-summary -o coverage.html --root "$PWD/../" $PWD/../build/src

