#include <iostream>
#include <format>

#include <TruthTableCreator/syntax_analysis/token_type.hpp>
#include <TruthTableCreator/syntax_analysis/token.hpp>
#include <TruthTableCreator/syntax_analysis/lexer.hpp>
#include <TruthTableCreator/syntax_analysis/parser.hpp>
#include <TruthTableCreator/evaluator/evaluator.hpp>

#include <TruthTableCreator/exceptions/unexpected_eof_exception.hpp>
#include <TruthTableCreator/exceptions/no_matching_token_type_exception.hpp>
#include <TruthTableCreator/exceptions/file_error_exception.hpp>

#define DEBUG(x) do {if (debugging) { std::cerr << x << std::endl; }} while(0);

int getMaxVarSize(std::vector<std::string> variables)
{
    int max_variable_length = 6; // Printing at least "Result"
    for(const std::string& variable : variables)
    {
        max_variable_length = (max_variable_length < variable.size()) ? variable.size() : max_variable_length;

    }

    return max_variable_length;
}

void printHeader(std::vector<std::string> variables, int col_size)
{
    int line_size = (1 + variables.size()) * (col_size + 1) + 1; //(result + variable) * (col_size + delimeter) + final_delimeter
    for(const std::string& variable : variables)
    {
        std::cout << "|" << std::format("{:>{}}", variable, col_size);
    }
    std:: cout << "|" << std::format("{:>{}}", "Result", col_size) << "|" << std::endl;;

    for(int i = 0; i < line_size; ++i)
    {
        std::cout << "=";
    }
    std::cout << std::endl;
}

void printRow(std::vector<bool> values, bool result, int col_size)
{
    for(const bool value: values)
    {
        std::cout << "|" << std::format("{:>{}}", std::to_string(value), col_size);
    }
    std:: cout << "|" << std::format("{:>{}}", std::to_string(result), col_size) << "|" << std::endl;
}

void printTable(std::map<std::map<std::string, bool>, bool> results)
{
    std::vector<std::string> variables = std::vector<std::string>();
    for(const auto& [variable_map, result] : results)
    {
        for(const auto& [variable, value] : variable_map)
        {
            variables.push_back(variable);
        }
        break;
    }

    int col_size = getMaxVarSize(variables);
    printHeader(variables, col_size);

    for(const auto& [variable_map, result] : results)
    {
        std::vector<bool> values = std::vector<bool>();
        for(const auto& [variable, value] : variable_map)
        {
            values.push_back(value);
        }
        printRow(values, result, col_size);
    }
}

std::string help_text = 
    "Usage: ttc [OPTION ...] PATH \n\n"
    "Options: \n"
    "  -h:\t shows this menu";  

bool debugging = false; // debug flag
int main(int argc, char** argv)
{
   try
    {
        if(argc < 2)
        {
            throw RuntimeErrorException("Please provide the path to a File!");
        }

        // Iterate over arguments and gather options (last argument should
        // always be the file path
        std::vector<std::string> options;
        int i = 0;
        while( i < argc - 1)
        {
            options.push_back(argv[i]);
            ++i;
        }

        // Get the Filepath
        std::string filepath = argv[i];

        if(filepath == "-h")
        {
            std::cout << help_text << std::endl;
            return 0; // end early if requesting help
        }

        for(const std::string& option: options)
        {
            if(option == "-h")
            {
                std::cout << help_text << std::endl;
                return 0; // end early if requesting help
            }
            if(option == "-d")
            {
                debugging = true;
                DEBUG("Debugging has been enabled!")
            }
        }
        DEBUG("Filepath: " + filepath);


        // Lexing
        std::ifstream myFile (filepath);
        Lexer lex = Lexer(&myFile);
        std::deque<Token> tokens = lex.scan();

        // Parsing
        Parser par = Parser(tokens);
        std::list<std::shared_ptr<Expression>> logical_expressions = par.parse();

        for(const std::shared_ptr<Expression>& logical_expression : logical_expressions)
        {
            std::cout << "The Truth Table for " << logical_expression -> toPrettyString() << " is: " << std::endl;
            std::map<std::map<std::string, bool>, bool> result = Evaluator::evaluateExpression(logical_expression);
            printTable(result);
            std::cout << std::endl;
        }


    } catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
        return EXIT_FAILURE;
    }

    return 0;
}
