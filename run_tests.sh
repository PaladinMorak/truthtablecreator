#!/bin/bash
set -e

__print_banner()
{
   echo "========================================"
   echo "$*"
   echo "========================================"
}

__print_banner "TEST TOKEN"
./build/tests/syntax_analysis/test_token

__print_banner "TEST TOKEN TYPE"
./build/tests/syntax_analysis/test_token_type

__print_banner "TEST Lexer"
./build/tests/syntax_analysis/test_lexer

__print_banner "TEST SourceLocation"
./build/tests/syntax/test_source_location

__print_banner "TEST AstNode"
./build/tests/syntax/test_ast_node

__print_banner "TEST Expression"
./build/tests/syntax/test_expression

__print_banner "TEST Const Expression"
./build/tests/syntax/test_const_expression

__print_banner "TEST Unary Expression"
./build/tests/syntax/test_unary_expression

__print_banner "TEST Not Expression"
./build/tests/syntax/test_not_expression

__print_banner "TEST Binary Expression"
./build/tests/syntax/test_binary_expression

__print_banner "TEST And Expression"
./build/tests/syntax/test_and_expression

__print_banner "TEST Biconditional Expression"
./build/tests/syntax/test_biconditional_expression

__print_banner "TEST Implication Expression"
./build/tests/syntax/test_implication_expression

__print_banner "TEST Or Expression"
./build/tests/syntax/test_or_expression

__print_banner "TEST Variable"
./build/tests/syntax/test_variable

__print_banner "TEST Node Type"
./build/tests/syntax/test_node_type

__print_banner "TEST Parser"
./build/tests/syntax_analysis/test_parser

__print_banner "TEST Evaluator"
./build/tests/evaluator/test_evaluator
